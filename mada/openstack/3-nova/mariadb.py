from mad import *
import time

class MariaDB(Component):

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', self.pull),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'common': (DepType.DATA_USE, ['deploy']),
            'haproxy': (DepType.DATA_USE, ['deploy']),
            'mdbd': (DepType.DATA_PROVIDE, ['initiated']),
            'mariadb': (DepType.DATA_PROVIDE, ['deployed'])
        }

    def pull(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(0.5)
