from mad import *
import time


class OpenVSwitch(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.DATA_USE, ['deploy'])
        }

    def deploy(self):
        time.sleep(1)
