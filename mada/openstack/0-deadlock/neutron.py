from mad import *
import time


class Neutron(Component):

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('initiated', 'pulled', self.config),
            'register': ('initiated', 'pulled', self.register),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.DATA_USE, ['config']),
            'mdbd': (DepType.DATA_USE, ['config']),
            'kstd': (DepType.DATA_USE, ['config', 'register']),
            'keystone': (DepType.DATA_USE, ['register']),
            'rabd': (DepType.DATA_USE, ['config']),
            'neud': (DepType.DATA_PROVIDE, ['initiated'])
        }

    def pull(self):
        time.sleep(1)

    def config(self):
        time.sleep(1)

    def register(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(1)
