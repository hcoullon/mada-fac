from mad import *
import time


class Nova(Component):

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'ready',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('initiated', 'pulled', self.config),
            'create_db': ('initiated', 'pulled', self.create_db),
            'register': ('initiated', 'deployed', self.register),
            'upgrade_db': ('pulled', 'ready', self.upgrade_db),
            'upgrade_api_db': ('pulled', 'ready', self.upgrade_api_db),
            'deploy': ('ready', 'deployed', self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.DATA_USE, ['create_db', 'upgrade_db',
                                     'upgrade_api_db']),
            'mdbd': (DepType.DATA_USE, ['create_db', 'upgrade_db',
                                      'upgrade_api_db']),
            'kstd': (DepType.DATA_USE, ['config', 'register']),
            'keystone': (DepType.DATA_USE, ['register']),
            'rabd': (DepType.DATA_USE, ['config']),
            'glad': (DepType.DATA_USE, ['config']),
            'novad': (DepType.DATA_PROVIDE, ['initiated'])
        }

    def pull(self):
        time.sleep(1)

    def config(self):
        time.sleep(1)

    def create_db(self):
        time.sleep(1.5)

    def register(self):
        time.sleep(1)

    def upgrade_db(self):
        time.sleep(1)

    def upgrade_api_db(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(1)
