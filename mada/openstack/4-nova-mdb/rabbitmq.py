from mad import *
import time


class RabbitMQ(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.DATA_USE, ['deploy']),
            'rabd': (DepType.DATA_PROVIDE, ['initiated'])
        }

    def deploy(self):
        time.sleep(1)

