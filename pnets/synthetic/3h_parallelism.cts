initially 
{
int c1_provide0 = 0;
int c1_provide1 = 0;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p1_od1 = 0;
int c1_p1_od2 = 0;
int c1_p2_id0 = 0;
int c1_p2_id1 = 0;
int c1_p2_id2 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_p3_od0 = 0;
int c1_p3_od1 = 0;
int c1_p3_od2 = 0;
int c1_p4_id0 = 0;
int c1_p4_id1 = 0;
int c1_p4_id2 = 0;
int c1_p4_id3 = 0;
int c1_p4 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c1_t3 = 0;
int c1_t4 = 0;
int c1_t5 = 0;
int c1_t6 = 0;
int c1_t7 = 0;
int c1_t8 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p0_od1 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p1_od1 = 0;
int c2_p1_od2 = 0;
int c2_p2_id0 = 0;
int c2_p2_id1 = 0;
int c2_p2_id2 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_p3_od0 = 0;
int c2_p3_od1 = 0;
int c2_p3_od2 = 0;
int c2_p4_id0 = 0;
int c2_p4_id1 = 0;
int c2_p4_id2 = 0;
int c2_p4_id3 = 0;
int c2_p4 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
int c2_t3 = 0;
int c2_t4 = 0;
int c2_t5 = 0;
int c2_t6 = 0;
int c2_t7 = 0;
int c2_t8 = 0;
int c3_provide0 = 0;
int c3_provide1 = 0;
int c3_p0 = 1;
int c3_p0_od0 = 0;
int c3_p0_od1 = 0;
int c3_p1_id0 = 0;
int c3_p1 = 0;
int c3_p1_od0 = 0;
int c3_p1_od1 = 0;
int c3_p1_od2 = 0;
int c3_p2_id0 = 0;
int c3_p2_id1 = 0;
int c3_p2_id2 = 0;
int c3_p2 = 0;
int c3_p2_od0 = 0;
int c3_p3_id0 = 0;
int c3_p3 = 0;
int c3_p3_od0 = 0;
int c3_p3_od1 = 0;
int c3_p3_od2 = 0;
int c3_p4_id0 = 0;
int c3_p4_id1 = 0;
int c3_p4_id2 = 0;
int c3_p4_id3 = 0;
int c3_p4 = 0;
int c3_t0 = 0;
int c3_t1 = 0;
int c3_t2 = 0;
int c3_t3 = 0;
int c3_t4 = 0;
int c3_t5 = 0;
int c3_t6 = 0;
int c3_t7 = 0;
int c3_t8 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1_od1=1; c1_p1_od2=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1 && c1_p2_id1==1 && c1_p2_id2==1){c1_p2_id0=0; c1_p2_id1=0; c1_p2_id2=0; c1_provide0=1; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p3_odocks [0,0]  when (c1_p3==1){c1_p3_od0=1; c1_p3_od1=1; c1_p3_od2=1; c1_p3=0;}
transition c1_p4_idocks [0,0]  when (c1_p4_id0==1 && c1_p4_id1==1 && c1_p4_id2==1 && c1_p4_id3==1){c1_p4_id0=0; c1_p4_id1=0; c1_p4_id2=0; c1_p4_id3=0; c1_provide1=1; c1_p4=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c3_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [32,37] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [41,43] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p1_od1_c1_t2 [0,0]  when (c1_p1_od1 == 1){c1_p1_od1=0; c1_t2=1;}
transition c1_p2_id1_c1_t2 [73,80] when (c1_t2 == 1){c1_p2_id1=1; c1_t2=0;}
transition c1_p1_od2_c1_t3 [0,0]  when (c1_p1_od2 == 1){c1_p1_od2=0; c1_t3=1;}
transition c1_p2_id2_c1_t3 [94,101] when (c1_t3 == 1){c1_p2_id2=1; c1_t3=0;}
transition c1_p2_od0_c1_t4 [0,0]  when (c1_p2_od0 == 1 && c2_provide1 >=1){c1_p2_od0=0; c1_t4=1;}
transition c1_p3_id0_c1_t4 [74,76] when (c1_t4 == 1){c1_p3_id0=1; c1_t4=0;}
transition c1_p3_od0_c1_t5 [0,0]  when (c1_p3_od0 == 1){c1_p3_od0=0; c1_t5=1;}
transition c1_p4_id0_c1_t5 [95,96] when (c1_t5 == 1){c1_p4_id0=1; c1_t5=0;}
transition c1_p3_od1_c1_t6 [0,0]  when (c1_p3_od1 == 1){c1_p3_od1=0; c1_t6=1;}
transition c1_p4_id1_c1_t6 [32,39] when (c1_t6 == 1){c1_p4_id1=1; c1_t6=0;}
transition c1_p3_od2_c1_t7 [0,0]  when (c1_p3_od2 == 1){c1_p3_od2=0; c1_t7=1;}
transition c1_p4_id2_c1_t7 [96,97] when (c1_t7 == 1){c1_p4_id2=1; c1_t7=0;}
transition c1_p0_od1_c1_t8 [0,0]  when (c1_p0_od1 == 1){c1_p0_od1=0; c1_t8=1;}
transition c1_p4_id3_c1_t8 [27,31] when (c1_t8 == 1){c1_p4_id3=1; c1_t8=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0_od1=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1_od1=1; c2_p1_od2=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1 && c2_p2_id1==1 && c2_p2_id2==1){c2_p2_id0=0; c2_p2_id1=0; c2_p2_id2=0; c2_provide0=1; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_p3=1;}
transition c2_p3_odocks [0,0]  when (c2_p3==1){c2_p3_od0=1; c2_p3_od1=1; c2_p3_od2=1; c2_p3=0;}
transition c2_p4_idocks [0,0]  when (c2_p4_id0==1 && c2_p4_id1==1 && c2_p4_id2==1 && c2_p4_id3==1){c2_p4_id0=0; c2_p4_id1=0; c2_p4_id2=0; c2_p4_id3=0; c2_provide1=1; c2_p4=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1 && c1_provide0 >=1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [76,81] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [40,45] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p1_od1_c2_t2 [0,0]  when (c2_p1_od1 == 1){c2_p1_od1=0; c2_t2=1;}
transition c2_p2_id1_c2_t2 [31,35] when (c2_t2 == 1){c2_p2_id1=1; c2_t2=0;}
transition c2_p1_od2_c2_t3 [0,0]  when (c2_p1_od2 == 1){c2_p1_od2=0; c2_t3=1;}
transition c2_p2_id2_c2_t3 [22,28] when (c2_t3 == 1){c2_p2_id2=1; c2_t3=0;}
transition c2_p2_od0_c2_t4 [0,0]  when (c2_p2_od0 == 1 && c3_provide1 >=1){c2_p2_od0=0; c2_t4=1;}
transition c2_p3_id0_c2_t4 [20,20] when (c2_t4 == 1){c2_p3_id0=1; c2_t4=0;}
transition c2_p3_od0_c2_t5 [0,0]  when (c2_p3_od0 == 1){c2_p3_od0=0; c2_t5=1;}
transition c2_p4_id0_c2_t5 [25,26] when (c2_t5 == 1){c2_p4_id0=1; c2_t5=0;}
transition c2_p3_od1_c2_t6 [0,0]  when (c2_p3_od1 == 1){c2_p3_od1=0; c2_t6=1;}
transition c2_p4_id1_c2_t6 [36,36] when (c2_t6 == 1){c2_p4_id1=1; c2_t6=0;}
transition c2_p3_od2_c2_t7 [0,0]  when (c2_p3_od2 == 1){c2_p3_od2=0; c2_t7=1;}
transition c2_p4_id2_c2_t7 [62,64] when (c2_t7 == 1){c2_p4_id2=1; c2_t7=0;}
transition c2_p0_od1_c2_t8 [0,0]  when (c2_p0_od1 == 1){c2_p0_od1=0; c2_t8=1;}
transition c2_p4_id3_c2_t8 [79,82] when (c2_t8 == 1){c2_p4_id3=1; c2_t8=0;}
transition c3_p0_odocks [0,0]  when (c3_p0==1){c3_p0_od0=1; c3_p0_od1=1; c3_p0=0;}
transition c3_p1_idocks [0,0]  when (c3_p1_id0==1){c3_p1_id0=0; c3_p1=1;}
transition c3_p1_odocks [0,0]  when (c3_p1==1){c3_p1_od0=1; c3_p1_od1=1; c3_p1_od2=1; c3_p1=0;}
transition c3_p2_idocks [0,0]  when (c3_p2_id0==1 && c3_p2_id1==1 && c3_p2_id2==1){c3_p2_id0=0; c3_p2_id1=0; c3_p2_id2=0; c3_provide0=1; c3_p2=1;}
transition c3_p2_odocks [0,0]  when (c3_p2==1){c3_p2_od0=1; c3_p2=0;}
transition c3_p3_idocks [0,0]  when (c3_p3_id0==1){c3_p3_id0=0; c3_p3=1;}
transition c3_p3_odocks [0,0]  when (c3_p3==1){c3_p3_od0=1; c3_p3_od1=1; c3_p3_od2=1; c3_p3=0;}
transition c3_p4_idocks [0,0]  when (c3_p4_id0==1 && c3_p4_id1==1 && c3_p4_id2==1 && c3_p4_id3==1){c3_p4_id0=0; c3_p4_id1=0; c3_p4_id2=0; c3_p4_id3=0; c3_provide1=1; c3_p4=1;}
transition c3_p0_od0_c3_t0 [0,0]  when (c3_p0_od0 == 1 && c2_provide0 >=1){c3_p0_od0=0; c3_t0=1;}
transition c3_p1_id0_c3_t0 [96,103] when (c3_t0 == 1){c3_p1_id0=1; c3_t0=0;}
transition c3_p1_od0_c3_t1 [0,0]  when (c3_p1_od0 == 1){c3_p1_od0=0; c3_t1=1;}
transition c3_p2_id0_c3_t1 [50,53] when (c3_t1 == 1){c3_p2_id0=1; c3_t1=0;}
transition c3_p1_od1_c3_t2 [0,0]  when (c3_p1_od1 == 1){c3_p1_od1=0; c3_t2=1;}
transition c3_p2_id1_c3_t2 [92,100] when (c3_t2 == 1){c3_p2_id1=1; c3_t2=0;}
transition c3_p1_od2_c3_t3 [0,0]  when (c3_p1_od2 == 1){c3_p1_od2=0; c3_t3=1;}
transition c3_p2_id2_c3_t3 [92,95] when (c3_t3 == 1){c3_p2_id2=1; c3_t3=0;}
transition c3_p2_od0_c3_t4 [0,0]  when (c3_p2_od0 == 1 && c1_provide1 >=1){c3_p2_od0=0; c3_t4=1;}
transition c3_p3_id0_c3_t4 [19,26] when (c3_t4 == 1){c3_p3_id0=1; c3_t4=0;}
transition c3_p3_od0_c3_t5 [0,0]  when (c3_p3_od0 == 1){c3_p3_od0=0; c3_t5=1;}
transition c3_p4_id0_c3_t5 [66,74] when (c3_t5 == 1){c3_p4_id0=1; c3_t5=0;}
transition c3_p3_od1_c3_t6 [0,0]  when (c3_p3_od1 == 1){c3_p3_od1=0; c3_t6=1;}
transition c3_p4_id1_c3_t6 [88,95] when (c3_t6 == 1){c3_p4_id1=1; c3_t6=0;}
transition c3_p3_od2_c3_t7 [0,0]  when (c3_p3_od2 == 1){c3_p3_od2=0; c3_t7=1;}
transition c3_p4_id2_c3_t7 [57,57] when (c3_t7 == 1){c3_p4_id2=1; c3_t7=0;}
transition c3_p0_od1_c3_t8 [0,0]  when (c3_p0_od1 == 1){c3_p0_od1=0; c3_t8=1;}
transition c3_p4_id3_c3_t8 [72,75] when (c3_t8 == 1){c3_p4_id3=1; c3_t8=0;}
check maxv (c1_t0 + c1_t1 + c1_t2 + c1_t3 + c1_t4 + c1_t5 + c1_t6 + c1_t7 + c1_t8 + c2_t0 + c2_t1 + c2_t2 + c2_t3 + c2_t4 + c2_t5 + c2_t6 + c2_t7 + c2_t8 + c3_t0 + c3_t1 + c3_t2 + c3_t3 + c3_t4 + c3_t5 + c3_t6 + c3_t7 + c3_t8)