initially 
{
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p0_od2 = 0;
int c1_p1_id0 = 0;
int c1_p1_id1 = 0;
int c1_p1_id2 = 0;
int c1_p1 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_provide2 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p2_id0 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0_od2=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1 && c1_p1_id1==1 && c1_p1_id2==1){c1_p1_id0=0; c1_p1_id1=0; c1_p1_id2=0; c1_p1=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c2_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [41,47] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p0_od1_c1_t1 [0,0]  when (c1_p0_od1 == 1 && c2_provide1 >=1){c1_p0_od1=0; c1_t1=1;}
transition c1_p1_id1_c1_t1 [27,29] when (c1_t1 == 1){c1_p1_id1=1; c1_t1=0;}
transition c1_p0_od2_c1_t2 [0,0]  when (c1_p0_od2 == 1 && c2_provide2 >=1){c1_p0_od2=0; c1_t2=1;}
transition c1_p1_id2_c1_t2 [29,36] when (c1_t2 == 1){c1_p1_id2=1; c1_t2=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_provide0=1; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1){c2_p2_id0=0; c2_provide1=1; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_provide2=1; c2_p3=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [63,72] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [28,36] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p2_od0_c2_t2 [0,0]  when (c2_p2_od0 == 1){c2_p2_od0=0; c2_t2=1;}
transition c2_p3_id0_c2_t2 [56,61] when (c2_t2 == 1){c2_p3_id0=1; c2_t2=0;}
cost_rate -1
check[neg_costs] mincost (c1_p1==1 && c2_p3==1)