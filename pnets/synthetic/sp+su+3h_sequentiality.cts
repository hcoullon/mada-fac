initially 
{
int badsequence = 0;
int c1_t4_fired = 0;
int c1_t4_neverfired = 1;
int c2_t4_fired = 0;
int c2_t4_neverfired = 1;
int c1_provide0 = 0;
int c1_provide1 = 0;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p1_od1 = 0;
int c1_p1_od2 = 0;
int c1_p2_id0 = 0;
int c1_p2_id1 = 0;
int c1_p2_id2 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_p3_od0 = 0;
int c1_p3_od1 = 0;
int c1_p3_od2 = 0;
int c1_p4_id0 = 0;
int c1_p4_id1 = 0;
int c1_p4_id2 = 0;
int c1_p4_id3 = 0;
int c1_p4 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c1_t3 = 0;
int c1_t4 = 0;
int c1_t5 = 0;
int c1_t6 = 0;
int c1_t7 = 0;
int c1_t8 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p0_od1 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p1_od1 = 0;
int c2_p1_od2 = 0;
int c2_p2_id0 = 0;
int c2_p2_id1 = 0;
int c2_p2_id2 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_p3_od0 = 0;
int c2_p3_od1 = 0;
int c2_p3_od2 = 0;
int c2_p4_id0 = 0;
int c2_p4_id1 = 0;
int c2_p4_id2 = 0;
int c2_p4_id3 = 0;
int c2_p4 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
int c2_t3 = 0;
int c2_t4 = 0;
int c2_t5 = 0;
int c2_t6 = 0;
int c2_t7 = 0;
int c2_t8 = 0;
int c3_provide0 = 0;
int c3_provide1 = 0;
int c3_p0 = 1;
int c3_p0_od0 = 0;
int c3_p0_od1 = 0;
int c3_p1_id0 = 0;
int c3_p1 = 0;
int c3_p1_od0 = 0;
int c3_p1_od1 = 0;
int c3_p1_od2 = 0;
int c3_p2_id0 = 0;
int c3_p2_id1 = 0;
int c3_p2_id2 = 0;
int c3_p2 = 0;
int c3_p2_od0 = 0;
int c3_p3_id0 = 0;
int c3_p3 = 0;
int c3_p3_od0 = 0;
int c3_p3_od1 = 0;
int c3_p3_od2 = 0;
int c3_p4_id0 = 0;
int c3_p4_id1 = 0;
int c3_p4_id2 = 0;
int c3_p4_id3 = 0;
int c3_p4 = 0;
int c3_t0 = 0;
int c3_t1 = 0;
int c3_t2 = 0;
int c3_t3 = 0;
int c3_t4 = 0;
int c3_t5 = 0;
int c3_t6 = 0;
int c3_t7 = 0;
int c3_t8 = 0;
int c4_provide0 = 0;
int c4_provide1 = 0;
int c4_p0 = 1;
int c4_p0_od0 = 0;
int c4_p1_id0 = 0;
int c4_p1 = 0;
int c4_p1_od0 = 0;
int c4_p2_id0 = 0;
int c4_p2 = 0;
int c4_t0 = 0;
int c4_t1 = 0;
int c5_p0 = 1;
int c5_p0_od0 = 0;
int c5_p1_id0 = 0;
int c5_p1 = 0;
int c5_p1_od0 = 0;
int c5_p2_id0 = 0;
int c5_p2 = 0;
int c5_t0 = 0;
int c5_t1 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1_od1=1; c1_p1_od2=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1 && c1_p2_id1==1 && c1_p2_id2==1){c1_p2_id0=0; c1_p2_id1=0; c1_p2_id2=0; c1_provide0=1; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p3_odocks [0,0]  when (c1_p3==1){c1_p3_od0=1; c1_p3_od1=1; c1_p3_od2=1; c1_p3=0;}
transition c1_p4_idocks [0,0]  when (c1_p4_id0==1 && c1_p4_id1==1 && c1_p4_id2==1 && c1_p4_id3==1){c1_p4_id0=0; c1_p4_id1=0; c1_p4_id2=0; c1_p4_id3=0; c1_provide1=1; c1_p4=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c2_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [91,99] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [60,63] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p1_od1_c1_t2 [0,0]  when (c1_p1_od1 == 1){c1_p1_od1=0; c1_t2=1;}
transition c1_p2_id1_c1_t2 [65,68] when (c1_t2 == 1){c1_p2_id1=1; c1_t2=0;}
transition c1_p1_od2_c1_t3 [0,0]  when (c1_p1_od2 == 1){c1_p1_od2=0; c1_t3=1;}
transition c1_p2_id2_c1_t3 [68,74] when (c1_t3 == 1){c1_p2_id2=1; c1_t3=0;}
transition c1_p2_od0_c1_t4 [0,0]  when (c1_p2_od0 == 1 && c3_provide1 >=1){c1_p2_od0=0; c1_t4=1;}
transition c1_p3_id0_c1_t4 [99,99] when (c1_t4 == 1){c1_t4_fired=1; c1_t4_neverfired=0; c1_p3_id0=1; c1_t4=0;}
transition c1_p3_od0_c1_t5 [0,0]  when (c1_p3_od0 == 1){c1_p3_od0=0; c1_t5=1;}
transition c1_p4_id0_c1_t5 [18,18] when (c1_t5 == 1){c1_p4_id0=1; c1_t5=0;}
transition c1_p3_od1_c1_t6 [0,0]  when (c1_p3_od1 == 1){c1_p3_od1=0; c1_t6=1;}
transition c1_p4_id1_c1_t6 [65,74] when (c1_t6 == 1){c1_p4_id1=1; c1_t6=0;}
transition c1_p3_od2_c1_t7 [0,0]  when (c1_p3_od2 == 1){c1_p3_od2=0; c1_t7=1;}
transition c1_p4_id2_c1_t7 [32,37] when (c1_t7 == 1){c1_p4_id2=1; c1_t7=0;}
transition c1_p0_od1_c1_t8 [0,0]  when (c1_p0_od1 == 1){c1_p0_od1=0; c1_t8=1;}
transition c1_p4_id3_c1_t8 [81,86] when (c1_t8 == 1){c1_p4_id3=1; c1_t8=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0_od1=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1_od1=1; c2_p1_od2=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1 && c2_p2_id1==1 && c2_p2_id2==1){c2_p2_id0=0; c2_p2_id1=0; c2_p2_id2=0; c2_provide0=1; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_p3=1;}
transition c2_p3_odocks [0,0]  when (c2_p3==1){c2_p3_od0=1; c2_p3_od1=1; c2_p3_od2=1; c2_p3=0;}
transition c2_p4_idocks [0,0]  when (c2_p4_id0==1 && c2_p4_id1==1 && c2_p4_id2==1 && c2_p4_id3==1){c2_p4_id0=0; c2_p4_id1=0; c2_p4_id2=0; c2_p4_id3=0; c2_provide1=1; c2_p4=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1 && c1_provide0 >=1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [97,99] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [55,58] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p1_od1_c2_t2 [0,0]  when (c2_p1_od1 == 1){c2_p1_od1=0; c2_t2=1;}
transition c2_p2_id1_c2_t2 [12,12] when (c2_t2 == 1){c2_p2_id1=1; c2_t2=0;}
transition c2_p1_od2_c2_t3 [0,0]  when (c2_p1_od2 == 1){c2_p1_od2=0; c2_t3=1;}
transition c2_p2_id2_c2_t3 [6,6] when (c2_t3 == 1){c2_p2_id2=1; c2_t3=0;}
transition c2_p2_od0_c2_t4 [0,0]  when (c2_p2_od0 == 1 && c4_provide1 >=1){c2_p2_od0=0; c2_t4=1;}
transition c2_p3_id0_c2_t4 [34,36] when (c2_t4 == 1){c2_t4_fired=1; c2_t4_neverfired=0; c2_p3_id0=1; c2_t4=0;}
transition c2_p3_od0_c2_t5 [0,0]  when (c2_p3_od0 == 1){c2_p3_od0=0; c2_t5=1;}
transition c2_p4_id0_c2_t5 [69,73] when (c2_t5 == 1){c2_p4_id0=1; c2_t5=0;}
transition c2_p3_od1_c2_t6 [0,0]  when (c2_p3_od1 == 1){c2_p3_od1=0; c2_t6=1;}
transition c2_p4_id1_c2_t6 [58,66] when (c2_t6 == 1){c2_p4_id1=1; c2_t6=0;}
transition c2_p3_od2_c2_t7 [0,0]  when (c2_p3_od2 == 1){c2_p3_od2=0; c2_t7=1;}
transition c2_p4_id2_c2_t7 [65,65] when (c2_t7 == 1){c2_p4_id2=1; c2_t7=0;}
transition c2_p0_od1_c2_t8 [0,0]  when (c2_p0_od1 == 1){c2_p0_od1=0; c2_t8=1;}
transition c2_p4_id3_c2_t8 [88,88] when (c2_t8 == 1){c2_p4_id3=1; c2_t8=0;}
transition c3_p0_odocks [0,0]  when (c3_p0==1){c3_p0_od0=1; c3_p0_od1=1; c3_p0=0;}
transition c3_p1_idocks [0,0]  when (c3_p1_id0==1){c3_p1_id0=0; c3_p1=1;}
transition c3_p1_odocks [0,0]  when (c3_p1==1){c3_p1_od0=1; c3_p1_od1=1; c3_p1_od2=1; c3_p1=0;}
transition c3_p2_idocks [0,0]  when (c3_p2_id0==1 && c3_p2_id1==1 && c3_p2_id2==1){c3_p2_id0=0; c3_p2_id1=0; c3_p2_id2=0; c3_provide0=1; c3_p2=1;}
transition c3_p2_odocks [0,0]  when (c3_p2==1){c3_p2_od0=1; c3_p2=0;}
transition c3_p3_idocks [0,0]  when (c3_p3_id0==1){c3_p3_id0=0; c3_p3=1;}
transition c3_p3_odocks [0,0]  when (c3_p3==1){c3_p3_od0=1; c3_p3_od1=1; c3_p3_od2=1; c3_p3=0;}
transition c3_p4_idocks [0,0]  when (c3_p4_id0==1 && c3_p4_id1==1 && c3_p4_id2==1 && c3_p4_id3==1){c3_p4_id0=0; c3_p4_id1=0; c3_p4_id2=0; c3_p4_id3=0; c3_provide1=1; c3_p4=1;}
transition c3_p0_od0_c3_t0 [0,0]  when (c3_p0_od0 == 1 && c1_provide1 >=1){c3_p0_od0=0; c3_t0=1;}
transition c3_p1_id0_c3_t0 [30,30] when (c3_t0 == 1){c3_p1_id0=1; c3_t0=0;}
transition c3_p1_od0_c3_t1 [0,0]  when (c3_p1_od0 == 1){c3_p1_od0=0; c3_t1=1;}
transition c3_p2_id0_c3_t1 [91,91] when (c3_t1 == 1){c3_p2_id0=1; c3_t1=0;}
transition c3_p1_od1_c3_t2 [0,0]  when (c3_p1_od1 == 1){c3_p1_od1=0; c3_t2=1;}
transition c3_p2_id1_c3_t2 [6,12] when (c3_t2 == 1){c3_p2_id1=1; c3_t2=0;}
transition c3_p1_od2_c3_t3 [0,0]  when (c3_p1_od2 == 1){c3_p1_od2=0; c3_t3=1;}
transition c3_p2_id2_c3_t3 [7,16] when (c3_t3 == 1){c3_p2_id2=1; c3_t3=0;}
transition c3_p2_od0_c3_t4 [0,0]  when (c3_p2_od0 == 1 && c2_provide1 >=1){c3_p2_od0=0; c3_t4=1;}
transition c3_p3_id0_c3_t4 [4,5] when (c3_t4 == 1){c3_p3_id0=1; c3_t4=0;}
transition c3_p3_od0_c3_t5 [0,0]  when (c3_p3_od0 == 1){c3_p3_od0=0; c3_t5=1;}
transition c3_p4_id0_c3_t5 [52,52] when (c3_t5 == 1){c3_p4_id0=1; c3_t5=0;}
transition c3_p3_od1_c3_t6 [0,0]  when (c3_p3_od1 == 1){c3_p3_od1=0; c3_t6=1;}
transition c3_p4_id1_c3_t6 [31,35] when (c3_t6 == 1){c3_p4_id1=1; c3_t6=0;}
transition c3_p3_od2_c3_t7 [0,0]  when (c3_p3_od2 == 1){c3_p3_od2=0; c3_t7=1;}
transition c3_p4_id2_c3_t7 [75,79] when (c3_t7 == 1){c3_p4_id2=1; c3_t7=0;}
transition c3_p0_od1_c3_t8 [0,0]  when (c3_p0_od1 == 1){c3_p0_od1=0; c3_t8=1;}
transition c3_p4_id3_c3_t8 [45,46] when (c3_t8 == 1){c3_p4_id3=1; c3_t8=0;}
transition c4_p0_odocks [0,0]  when (c4_p0==1){c4_p0_od0=1; c4_p0=0;}
transition c4_p1_idocks [0,0]  when (c4_p1_id0==1){c4_p1_id0=0; c4_provide0=1; c4_p1=1;}
transition c4_p1_odocks [0,0]  when (c4_p1==1){c4_p1_od0=1; c4_p1=0;}
transition c4_p2_idocks [0,0]  when (c4_p2_id0==1){c4_p2_id0=0; c4_provide1=1; c4_p2=1;}
transition c4_p0_od0_c4_t0 [0,0]  when (c4_p0_od0 == 1){c4_p0_od0=0; c4_t0=1;}
transition c4_p1_id0_c4_t0 [6,6] when (c4_t0 == 1){c4_p1_id0=1; c4_t0=0;}
transition c4_p1_od0_c4_t1 [0,0]  when (c4_p1_od0 == 1){c4_p1_od0=0; c4_t1=1;}
transition c4_p2_id0_c4_t1 [62,62] when (c4_t1 == 1){c4_p2_id0=1; c4_t1=0;}
transition c5_p0_odocks [0,0]  when (c5_p0==1){c5_p0_od0=1; c5_p0=0;}
transition c5_p1_idocks [0,0]  when (c5_p1_id0==1){c5_p1_id0=0; c5_p1=1;}
transition c5_p1_odocks [0,0]  when (c5_p1==1){c5_p1_od0=1; c5_p1=0;}
transition c5_p2_idocks [0,0]  when (c5_p2_id0==1){c5_p2_id0=0; c5_p2=1;}
transition c5_p0_od0_c5_t0 [0,0]  when (c5_p0_od0 == 1 && c3_provide0 >=1){c5_p0_od0=0; c5_t0=1;}
transition c5_p1_id0_c5_t0 [15,18] when (c5_t0 == 1){c5_p1_id0=1; c5_t0=0;}
transition c5_p1_od0_c5_t1 [0,0]  when (c5_p1_od0 == 1 && c4_provide0 >=1){c5_p1_od0=0; c5_t1=1;}
transition c5_p2_id0_c5_t1 [52,59] when (c5_t1 == 1){c5_p2_id0=1; c5_t1=0;}
transition c2_t4_c1_t4_order [0,0]  when (c2_t4_neverfired == 1 && c1_t4_fired == 1){badsequence=1;}
check AG (badsequence==0)
