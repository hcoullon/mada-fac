initially 
{
int c1_provide0 = 0;
int c1_provide1 = 0;
int c1_provide2 = 0;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p1_od1 = 0;
int c1_p1_od2 = 0;
int c1_p2_id0 = 0;
int c1_p2_id1 = 0;
int c1_p2_id2 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_p3_od0 = 0;
int c1_p3_od1 = 0;
int c1_p3_od2 = 0;
int c1_p4_id0 = 0;
int c1_p4_id1 = 0;
int c1_p4_id2 = 0;
int c1_p4 = 0;
int c1_p4_od0 = 0;
int c1_p5_id0 = 0;
int c1_p5 = 0;
int c1_p5_od0 = 0;
int c1_p5_od1 = 0;
int c1_p5_od2 = 0;
int c1_p6_id0 = 0;
int c1_p6_id1 = 0;
int c1_p6_id2 = 0;
int c1_p6_id3 = 0;
int c1_p6 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c1_t3 = 0;
int c1_t4 = 0;
int c1_t5 = 0;
int c1_t6 = 0;
int c1_t7 = 0;
int c1_t8 = 0;
int c1_t9 = 0;
int c1_t10 = 0;
int c1_t11 = 0;
int c1_t12 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p0_od1 = 0;
int c2_p0_od2 = 0;
int c2_p1_id0 = 0;
int c2_p1_id1 = 0;
int c2_p1_id2 = 0;
int c2_p1 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
int c3_provide0 = 0;
int c3_provide1 = 0;
int c3_provide2 = 0;
int c3_p0 = 1;
int c3_p0_od0 = 0;
int c3_p0_od1 = 0;
int c3_p0_od2 = 0;
int c3_p1_id0 = 0;
int c3_p1 = 0;
int c3_p2_id0 = 0;
int c3_p2 = 0;
int c3_p3_id0 = 0;
int c3_p3 = 0;
int c3_t0 = 0;
int c3_t1 = 0;
int c3_t2 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1_od1=1; c1_p1_od2=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1 && c1_p2_id1==1 && c1_p2_id2==1){c1_p2_id0=0; c1_p2_id1=0; c1_p2_id2=0; c1_provide0=1; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p3_odocks [0,0]  when (c1_p3==1){c1_p3_od0=1; c1_p3_od1=1; c1_p3_od2=1; c1_p3=0;}
transition c1_p4_idocks [0,0]  when (c1_p4_id0==1 && c1_p4_id1==1 && c1_p4_id2==1){c1_p4_id0=0; c1_p4_id1=0; c1_p4_id2=0; c1_provide1=1; c1_p4=1;}
transition c1_p4_odocks [0,0]  when (c1_p4==1){c1_p4_od0=1; c1_p4=0;}
transition c1_p5_idocks [0,0]  when (c1_p5_id0==1){c1_p5_id0=0; c1_p5=1;}
transition c1_p5_odocks [0,0]  when (c1_p5==1){c1_p5_od0=1; c1_p5_od1=1; c1_p5_od2=1; c1_p5=0;}
transition c1_p6_idocks [0,0]  when (c1_p6_id0==1 && c1_p6_id1==1 && c1_p6_id2==1 && c1_p6_id3==1){c1_p6_id0=0; c1_p6_id1=0; c1_p6_id2=0; c1_p6_id3=0; c1_provide2=1; c1_p6=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c3_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [33,42] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [32,34] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p1_od1_c1_t2 [0,0]  when (c1_p1_od1 == 1){c1_p1_od1=0; c1_t2=1;}
transition c1_p2_id1_c1_t2 [75,76] when (c1_t2 == 1){c1_p2_id1=1; c1_t2=0;}
transition c1_p1_od2_c1_t3 [0,0]  when (c1_p1_od2 == 1){c1_p1_od2=0; c1_t3=1;}
transition c1_p2_id2_c1_t3 [91,91] when (c1_t3 == 1){c1_p2_id2=1; c1_t3=0;}
transition c1_p2_od0_c1_t4 [0,0]  when (c1_p2_od0 == 1 && c3_provide2 >=1){c1_p2_od0=0; c1_t4=1;}
transition c1_p3_id0_c1_t4 [35,35] when (c1_t4 == 1){c1_p3_id0=1; c1_t4=0;}
transition c1_p3_od0_c1_t5 [0,0]  when (c1_p3_od0 == 1){c1_p3_od0=0; c1_t5=1;}
transition c1_p4_id0_c1_t5 [9,17] when (c1_t5 == 1){c1_p4_id0=1; c1_t5=0;}
transition c1_p3_od1_c1_t6 [0,0]  when (c1_p3_od1 == 1){c1_p3_od1=0; c1_t6=1;}
transition c1_p4_id1_c1_t6 [68,75] when (c1_t6 == 1){c1_p4_id1=1; c1_t6=0;}
transition c1_p3_od2_c1_t7 [0,0]  when (c1_p3_od2 == 1){c1_p3_od2=0; c1_t7=1;}
transition c1_p4_id2_c1_t7 [64,69] when (c1_t7 == 1){c1_p4_id2=1; c1_t7=0;}
transition c1_p4_od0_c1_t8 [0,0]  when (c1_p4_od0 == 1 && c3_provide1 >=1){c1_p4_od0=0; c1_t8=1;}
transition c1_p5_id0_c1_t8 [92,98] when (c1_t8 == 1){c1_p5_id0=1; c1_t8=0;}
transition c1_p5_od0_c1_t9 [0,0]  when (c1_p5_od0 == 1){c1_p5_od0=0; c1_t9=1;}
transition c1_p6_id0_c1_t9 [54,60] when (c1_t9 == 1){c1_p6_id0=1; c1_t9=0;}
transition c1_p5_od1_c1_t10 [0,0]  when (c1_p5_od1 == 1){c1_p5_od1=0; c1_t10=1;}
transition c1_p6_id1_c1_t10 [79,88] when (c1_t10 == 1){c1_p6_id1=1; c1_t10=0;}
transition c1_p5_od2_c1_t11 [0,0]  when (c1_p5_od2 == 1){c1_p5_od2=0; c1_t11=1;}
transition c1_p6_id2_c1_t11 [65,66] when (c1_t11 == 1){c1_p6_id2=1; c1_t11=0;}
transition c1_p0_od1_c1_t12 [0,0]  when (c1_p0_od1 == 1){c1_p0_od1=0; c1_t12=1;}
transition c1_p6_id3_c1_t12 [30,38] when (c1_t12 == 1){c1_p6_id3=1; c1_t12=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0_od1=1; c2_p0_od2=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1 && c2_p1_id1==1 && c2_p1_id2==1){c2_p1_id0=0; c2_p1_id1=0; c2_p1_id2=0; c2_p1=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1 && c1_provide0 >=1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [3,4] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p0_od1_c2_t1 [0,0]  when (c2_p0_od1 == 1 && c1_provide1 >=1){c2_p0_od1=0; c2_t1=1;}
transition c2_p1_id1_c2_t1 [30,36] when (c2_t1 == 1){c2_p1_id1=1; c2_t1=0;}
transition c2_p0_od2_c2_t2 [0,0]  when (c2_p0_od2 == 1 && c1_provide2 >=1){c2_p0_od2=0; c2_t2=1;}
transition c2_p1_id2_c2_t2 [98,99] when (c2_t2 == 1){c2_p1_id2=1; c2_t2=0;}
transition c3_p0_odocks [0,0]  when (c3_p0==1){c3_p0_od0=1; c3_p0_od1=1; c3_p0_od2=1; c3_p0=0;}
transition c3_p1_idocks [0,0]  when (c3_p1_id0==1){c3_p1_id0=0; c3_provide0=1; c3_p1=1;}
transition c3_p2_idocks [0,0]  when (c3_p2_id0==1){c3_p2_id0=0; c3_provide1=1; c3_p2=1;}
transition c3_p3_idocks [0,0]  when (c3_p3_id0==1){c3_p3_id0=0; c3_provide2=1; c3_p3=1;}
transition c3_p0_od0_c3_t0 [0,0]  when (c3_p0_od0 == 1){c3_p0_od0=0; c3_t0=1;}
transition c3_p1_id0_c3_t0 [89,92] when (c3_t0 == 1){c3_p1_id0=1; c3_t0=0;}
transition c3_p0_od1_c3_t1 [0,0]  when (c3_p0_od1 == 1){c3_p0_od1=0; c3_t1=1;}
transition c3_p2_id0_c3_t1 [28,37] when (c3_t1 == 1){c3_p2_id0=1; c3_t1=0;}
transition c3_p0_od2_c3_t2 [0,0]  when (c3_p0_od2 == 1){c3_p0_od2=0; c3_t2=1;}
transition c3_p3_id0_c3_t2 [19,25] when (c3_t2 == 1){c3_p3_id0=1; c3_t2=0;}
cost_rate -1
check[neg_costs] mincost (c1_p6==1 && c2_p1==1 && c3_p1==1 && c3_p2==1 && c3_p3==1)