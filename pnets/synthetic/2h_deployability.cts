initially 
{
int c1_provide0 = 0;
int c1_provide1 = 0;
int c1_provide2 = 0;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p1_od1 = 0;
int c1_p1_od2 = 0;
int c1_p2_id0 = 0;
int c1_p2_id1 = 0;
int c1_p2_id2 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_p3_od0 = 0;
int c1_p3_od1 = 0;
int c1_p3_od2 = 0;
int c1_p4_id0 = 0;
int c1_p4_id1 = 0;
int c1_p4_id2 = 0;
int c1_p4 = 0;
int c1_p4_od0 = 0;
int c1_p5_id0 = 0;
int c1_p5 = 0;
int c1_p5_od0 = 0;
int c1_p5_od1 = 0;
int c1_p5_od2 = 0;
int c1_p6_id0 = 0;
int c1_p6_id1 = 0;
int c1_p6_id2 = 0;
int c1_p6_id3 = 0;
int c1_p6 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c1_t3 = 0;
int c1_t4 = 0;
int c1_t5 = 0;
int c1_t6 = 0;
int c1_t7 = 0;
int c1_t8 = 0;
int c1_t9 = 0;
int c1_t10 = 0;
int c1_t11 = 0;
int c1_t12 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_provide2 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p0_od1 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p1_od1 = 0;
int c2_p1_od2 = 0;
int c2_p2_id0 = 0;
int c2_p2_id1 = 0;
int c2_p2_id2 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_p3_od0 = 0;
int c2_p3_od1 = 0;
int c2_p3_od2 = 0;
int c2_p4_id0 = 0;
int c2_p4_id1 = 0;
int c2_p4_id2 = 0;
int c2_p4 = 0;
int c2_p4_od0 = 0;
int c2_p5_id0 = 0;
int c2_p5 = 0;
int c2_p5_od0 = 0;
int c2_p5_od1 = 0;
int c2_p5_od2 = 0;
int c2_p6_id0 = 0;
int c2_p6_id1 = 0;
int c2_p6_id2 = 0;
int c2_p6_id3 = 0;
int c2_p6 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
int c2_t3 = 0;
int c2_t4 = 0;
int c2_t5 = 0;
int c2_t6 = 0;
int c2_t7 = 0;
int c2_t8 = 0;
int c2_t9 = 0;
int c2_t10 = 0;
int c2_t11 = 0;
int c2_t12 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1_od1=1; c1_p1_od2=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1 && c1_p2_id1==1 && c1_p2_id2==1){c1_p2_id0=0; c1_p2_id1=0; c1_p2_id2=0; c1_provide0=1; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p3_odocks [0,0]  when (c1_p3==1){c1_p3_od0=1; c1_p3_od1=1; c1_p3_od2=1; c1_p3=0;}
transition c1_p4_idocks [0,0]  when (c1_p4_id0==1 && c1_p4_id1==1 && c1_p4_id2==1){c1_p4_id0=0; c1_p4_id1=0; c1_p4_id2=0; c1_provide1=1; c1_p4=1;}
transition c1_p4_odocks [0,0]  when (c1_p4==1){c1_p4_od0=1; c1_p4=0;}
transition c1_p5_idocks [0,0]  when (c1_p5_id0==1){c1_p5_id0=0; c1_p5=1;}
transition c1_p5_odocks [0,0]  when (c1_p5==1){c1_p5_od0=1; c1_p5_od1=1; c1_p5_od2=1; c1_p5=0;}
transition c1_p6_idocks [0,0]  when (c1_p6_id0==1 && c1_p6_id1==1 && c1_p6_id2==1 && c1_p6_id3==1){c1_p6_id0=0; c1_p6_id1=0; c1_p6_id2=0; c1_p6_id3=0; c1_provide2=1; c1_p6=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c2_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [52,55] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [43,52] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p1_od1_c1_t2 [0,0]  when (c1_p1_od1 == 1){c1_p1_od1=0; c1_t2=1;}
transition c1_p2_id1_c1_t2 [34,43] when (c1_t2 == 1){c1_p2_id1=1; c1_t2=0;}
transition c1_p1_od2_c1_t3 [0,0]  when (c1_p1_od2 == 1){c1_p1_od2=0; c1_t3=1;}
transition c1_p2_id2_c1_t3 [97,100] when (c1_t3 == 1){c1_p2_id2=1; c1_t3=0;}
transition c1_p2_od0_c1_t4 [0,0]  when (c1_p2_od0 == 1 && c2_provide1 >=1){c1_p2_od0=0; c1_t4=1;}
transition c1_p3_id0_c1_t4 [19,26] when (c1_t4 == 1){c1_p3_id0=1; c1_t4=0;}
transition c1_p3_od0_c1_t5 [0,0]  when (c1_p3_od0 == 1){c1_p3_od0=0; c1_t5=1;}
transition c1_p4_id0_c1_t5 [98,103] when (c1_t5 == 1){c1_p4_id0=1; c1_t5=0;}
transition c1_p3_od1_c1_t6 [0,0]  when (c1_p3_od1 == 1){c1_p3_od1=0; c1_t6=1;}
transition c1_p4_id1_c1_t6 [53,53] when (c1_t6 == 1){c1_p4_id1=1; c1_t6=0;}
transition c1_p3_od2_c1_t7 [0,0]  when (c1_p3_od2 == 1){c1_p3_od2=0; c1_t7=1;}
transition c1_p4_id2_c1_t7 [81,90] when (c1_t7 == 1){c1_p4_id2=1; c1_t7=0;}
transition c1_p4_od0_c1_t8 [0,0]  when (c1_p4_od0 == 1 && c2_provide2 >=1){c1_p4_od0=0; c1_t8=1;}
transition c1_p5_id0_c1_t8 [83,85] when (c1_t8 == 1){c1_p5_id0=1; c1_t8=0;}
transition c1_p5_od0_c1_t9 [0,0]  when (c1_p5_od0 == 1){c1_p5_od0=0; c1_t9=1;}
transition c1_p6_id0_c1_t9 [50,51] when (c1_t9 == 1){c1_p6_id0=1; c1_t9=0;}
transition c1_p5_od1_c1_t10 [0,0]  when (c1_p5_od1 == 1){c1_p5_od1=0; c1_t10=1;}
transition c1_p6_id1_c1_t10 [49,58] when (c1_t10 == 1){c1_p6_id1=1; c1_t10=0;}
transition c1_p5_od2_c1_t11 [0,0]  when (c1_p5_od2 == 1){c1_p5_od2=0; c1_t11=1;}
transition c1_p6_id2_c1_t11 [97,103] when (c1_t11 == 1){c1_p6_id2=1; c1_t11=0;}
transition c1_p0_od1_c1_t12 [0,0]  when (c1_p0_od1 == 1){c1_p0_od1=0; c1_t12=1;}
transition c1_p6_id3_c1_t12 [69,71] when (c1_t12 == 1){c1_p6_id3=1; c1_t12=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0_od1=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1_od1=1; c2_p1_od2=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1 && c2_p2_id1==1 && c2_p2_id2==1){c2_p2_id0=0; c2_p2_id1=0; c2_p2_id2=0; c2_provide0=1; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_p3=1;}
transition c2_p3_odocks [0,0]  when (c2_p3==1){c2_p3_od0=1; c2_p3_od1=1; c2_p3_od2=1; c2_p3=0;}
transition c2_p4_idocks [0,0]  when (c2_p4_id0==1 && c2_p4_id1==1 && c2_p4_id2==1){c2_p4_id0=0; c2_p4_id1=0; c2_p4_id2=0; c2_provide1=1; c2_p4=1;}
transition c2_p4_odocks [0,0]  when (c2_p4==1){c2_p4_od0=1; c2_p4=0;}
transition c2_p5_idocks [0,0]  when (c2_p5_id0==1){c2_p5_id0=0; c2_p5=1;}
transition c2_p5_odocks [0,0]  when (c2_p5==1){c2_p5_od0=1; c2_p5_od1=1; c2_p5_od2=1; c2_p5=0;}
transition c2_p6_idocks [0,0]  when (c2_p6_id0==1 && c2_p6_id1==1 && c2_p6_id2==1 && c2_p6_id3==1){c2_p6_id0=0; c2_p6_id1=0; c2_p6_id2=0; c2_p6_id3=0; c2_provide2=1; c2_p6=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1 && c1_provide0 >=1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [21,27] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [32,37] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p1_od1_c2_t2 [0,0]  when (c2_p1_od1 == 1){c2_p1_od1=0; c2_t2=1;}
transition c2_p2_id1_c2_t2 [71,77] when (c2_t2 == 1){c2_p2_id1=1; c2_t2=0;}
transition c2_p1_od2_c2_t3 [0,0]  when (c2_p1_od2 == 1){c2_p1_od2=0; c2_t3=1;}
transition c2_p2_id2_c2_t3 [23,28] when (c2_t3 == 1){c2_p2_id2=1; c2_t3=0;}
transition c2_p2_od0_c2_t4 [0,0]  when (c2_p2_od0 == 1 && c1_provide1 >=1){c2_p2_od0=0; c2_t4=1;}
transition c2_p3_id0_c2_t4 [4,7] when (c2_t4 == 1){c2_p3_id0=1; c2_t4=0;}
transition c2_p3_od0_c2_t5 [0,0]  when (c2_p3_od0 == 1){c2_p3_od0=0; c2_t5=1;}
transition c2_p4_id0_c2_t5 [85,93] when (c2_t5 == 1){c2_p4_id0=1; c2_t5=0;}
transition c2_p3_od1_c2_t6 [0,0]  when (c2_p3_od1 == 1){c2_p3_od1=0; c2_t6=1;}
transition c2_p4_id1_c2_t6 [9,10] when (c2_t6 == 1){c2_p4_id1=1; c2_t6=0;}
transition c2_p3_od2_c2_t7 [0,0]  when (c2_p3_od2 == 1){c2_p3_od2=0; c2_t7=1;}
transition c2_p4_id2_c2_t7 [76,78] when (c2_t7 == 1){c2_p4_id2=1; c2_t7=0;}
transition c2_p4_od0_c2_t8 [0,0]  when (c2_p4_od0 == 1 && c1_provide2 >=1){c2_p4_od0=0; c2_t8=1;}
transition c2_p5_id0_c2_t8 [24,29] when (c2_t8 == 1){c2_p5_id0=1; c2_t8=0;}
transition c2_p5_od0_c2_t9 [0,0]  when (c2_p5_od0 == 1){c2_p5_od0=0; c2_t9=1;}
transition c2_p6_id0_c2_t9 [18,25] when (c2_t9 == 1){c2_p6_id0=1; c2_t9=0;}
transition c2_p5_od1_c2_t10 [0,0]  when (c2_p5_od1 == 1){c2_p5_od1=0; c2_t10=1;}
transition c2_p6_id1_c2_t10 [26,32] when (c2_t10 == 1){c2_p6_id1=1; c2_t10=0;}
transition c2_p5_od2_c2_t11 [0,0]  when (c2_p5_od2 == 1){c2_p5_od2=0; c2_t11=1;}
transition c2_p6_id2_c2_t11 [99,107] when (c2_t11 == 1){c2_p6_id2=1; c2_t11=0;}
transition c2_p0_od1_c2_t12 [0,0]  when (c2_p0_od1 == 1){c2_p0_od1=0; c2_t12=1;}
transition c2_p6_id3_c2_t12 [95,102] when (c2_t12 == 1){c2_p6_id3=1; c2_t12=0;}
check AF (c1_p6==1 && c2_p6==1)
