initially 
{
int c1_provide0 = 0;
int c1_provide1 = 0;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p1_od1 = 0;
int c1_p1_od2 = 0;
int c1_p2_id0 = 0;
int c1_p2_id1 = 0;
int c1_p2_id2 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_p3_od0 = 0;
int c1_p3_od1 = 0;
int c1_p3_od2 = 0;
int c1_p4_id0 = 0;
int c1_p4_id1 = 0;
int c1_p4_id2 = 0;
int c1_p4_id3 = 0;
int c1_p4 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c1_t3 = 0;
int c1_t4 = 0;
int c1_t5 = 0;
int c1_t6 = 0;
int c1_t7 = 0;
int c1_t8 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p0_od1 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p1_od1 = 0;
int c2_p1_od2 = 0;
int c2_p2_id0 = 0;
int c2_p2_id1 = 0;
int c2_p2_id2 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_p3_od0 = 0;
int c2_p3_od1 = 0;
int c2_p3_od2 = 0;
int c2_p4_id0 = 0;
int c2_p4_id1 = 0;
int c2_p4_id2 = 0;
int c2_p4_id3 = 0;
int c2_p4 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
int c2_t3 = 0;
int c2_t4 = 0;
int c2_t5 = 0;
int c2_t6 = 0;
int c2_t7 = 0;
int c2_t8 = 0;
int c3_p0 = 1;
int c3_p0_od0 = 0;
int c3_p0_od1 = 0;
int c3_p0_od2 = 0;
int c3_p1_id0 = 0;
int c3_p1_id1 = 0;
int c3_p1_id2 = 0;
int c3_p1 = 0;
int c3_t0 = 0;
int c3_t1 = 0;
int c3_t2 = 0;
int c4_provide0 = 0;
int c4_provide1 = 0;
int c4_provide2 = 0;
int c4_p0 = 1;
int c4_p0_od0 = 0;
int c4_p0_od1 = 0;
int c4_p0_od2 = 0;
int c4_p1_id0 = 0;
int c4_p1 = 0;
int c4_p2_id0 = 0;
int c4_p2 = 0;
int c4_p3_id0 = 0;
int c4_p3 = 0;
int c4_t0 = 0;
int c4_t1 = 0;
int c4_t2 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1_od1=1; c1_p1_od2=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1 && c1_p2_id1==1 && c1_p2_id2==1){c1_p2_id0=0; c1_p2_id1=0; c1_p2_id2=0; c1_provide0=1; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p3_odocks [0,0]  when (c1_p3==1){c1_p3_od0=1; c1_p3_od1=1; c1_p3_od2=1; c1_p3=0;}
transition c1_p4_idocks [0,0]  when (c1_p4_id0==1 && c1_p4_id1==1 && c1_p4_id2==1 && c1_p4_id3==1){c1_p4_id0=0; c1_p4_id1=0; c1_p4_id2=0; c1_p4_id3=0; c1_provide1=1; c1_p4=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c2_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [70,77] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [59,67] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p1_od1_c1_t2 [0,0]  when (c1_p1_od1 == 1){c1_p1_od1=0; c1_t2=1;}
transition c1_p2_id1_c1_t2 [95,102] when (c1_t2 == 1){c1_p2_id1=1; c1_t2=0;}
transition c1_p1_od2_c1_t3 [0,0]  when (c1_p1_od2 == 1){c1_p1_od2=0; c1_t3=1;}
transition c1_p2_id2_c1_t3 [38,43] when (c1_t3 == 1){c1_p2_id2=1; c1_t3=0;}
transition c1_p2_od0_c1_t4 [0,0]  when (c1_p2_od0 == 1 && c4_provide2 >=1){c1_p2_od0=0; c1_t4=1;}
transition c1_p3_id0_c1_t4 [96,98] when (c1_t4 == 1){c1_p3_id0=1; c1_t4=0;}
transition c1_p3_od0_c1_t5 [0,0]  when (c1_p3_od0 == 1){c1_p3_od0=0; c1_t5=1;}
transition c1_p4_id0_c1_t5 [24,33] when (c1_t5 == 1){c1_p4_id0=1; c1_t5=0;}
transition c1_p3_od1_c1_t6 [0,0]  when (c1_p3_od1 == 1){c1_p3_od1=0; c1_t6=1;}
transition c1_p4_id1_c1_t6 [43,45] when (c1_t6 == 1){c1_p4_id1=1; c1_t6=0;}
transition c1_p3_od2_c1_t7 [0,0]  when (c1_p3_od2 == 1){c1_p3_od2=0; c1_t7=1;}
transition c1_p4_id2_c1_t7 [30,36] when (c1_t7 == 1){c1_p4_id2=1; c1_t7=0;}
transition c1_p0_od1_c1_t8 [0,0]  when (c1_p0_od1 == 1){c1_p0_od1=0; c1_t8=1;}
transition c1_p4_id3_c1_t8 [64,65] when (c1_t8 == 1){c1_p4_id3=1; c1_t8=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0_od1=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1_od1=1; c2_p1_od2=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1 && c2_p2_id1==1 && c2_p2_id2==1){c2_p2_id0=0; c2_p2_id1=0; c2_p2_id2=0; c2_provide0=1; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_p3=1;}
transition c2_p3_odocks [0,0]  when (c2_p3==1){c2_p3_od0=1; c2_p3_od1=1; c2_p3_od2=1; c2_p3=0;}
transition c2_p4_idocks [0,0]  when (c2_p4_id0==1 && c2_p4_id1==1 && c2_p4_id2==1 && c2_p4_id3==1){c2_p4_id0=0; c2_p4_id1=0; c2_p4_id2=0; c2_p4_id3=0; c2_provide1=1; c2_p4=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1 && c1_provide0 >=1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [66,70] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [23,30] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p1_od1_c2_t2 [0,0]  when (c2_p1_od1 == 1){c2_p1_od1=0; c2_t2=1;}
transition c2_p2_id1_c2_t2 [77,85] when (c2_t2 == 1){c2_p2_id1=1; c2_t2=0;}
transition c2_p1_od2_c2_t3 [0,0]  when (c2_p1_od2 == 1){c2_p1_od2=0; c2_t3=1;}
transition c2_p2_id2_c2_t3 [82,87] when (c2_t3 == 1){c2_p2_id2=1; c2_t3=0;}
transition c2_p2_od0_c2_t4 [0,0]  when (c2_p2_od0 == 1 && c4_provide1 >=1){c2_p2_od0=0; c2_t4=1;}
transition c2_p3_id0_c2_t4 [85,86] when (c2_t4 == 1){c2_p3_id0=1; c2_t4=0;}
transition c2_p3_od0_c2_t5 [0,0]  when (c2_p3_od0 == 1){c2_p3_od0=0; c2_t5=1;}
transition c2_p4_id0_c2_t5 [62,71] when (c2_t5 == 1){c2_p4_id0=1; c2_t5=0;}
transition c2_p3_od1_c2_t6 [0,0]  when (c2_p3_od1 == 1){c2_p3_od1=0; c2_t6=1;}
transition c2_p4_id1_c2_t6 [46,49] when (c2_t6 == 1){c2_p4_id1=1; c2_t6=0;}
transition c2_p3_od2_c2_t7 [0,0]  when (c2_p3_od2 == 1){c2_p3_od2=0; c2_t7=1;}
transition c2_p4_id2_c2_t7 [75,77] when (c2_t7 == 1){c2_p4_id2=1; c2_t7=0;}
transition c2_p0_od1_c2_t8 [0,0]  when (c2_p0_od1 == 1){c2_p0_od1=0; c2_t8=1;}
transition c2_p4_id3_c2_t8 [43,48] when (c2_t8 == 1){c2_p4_id3=1; c2_t8=0;}
transition c3_p0_odocks [0,0]  when (c3_p0==1){c3_p0_od0=1; c3_p0_od1=1; c3_p0_od2=1; c3_p0=0;}
transition c3_p1_idocks [0,0]  when (c3_p1_id0==1 && c3_p1_id1==1 && c3_p1_id2==1){c3_p1_id0=0; c3_p1_id1=0; c3_p1_id2=0; c3_p1=1;}
transition c3_p0_od0_c3_t0 [0,0]  when (c3_p0_od0 == 1 && c4_provide0 >=1){c3_p0_od0=0; c3_t0=1;}
transition c3_p1_id0_c3_t0 [57,60] when (c3_t0 == 1){c3_p1_id0=1; c3_t0=0;}
transition c3_p0_od1_c3_t1 [0,0]  when (c3_p0_od1 == 1 && c1_provide1 >=1){c3_p0_od1=0; c3_t1=1;}
transition c3_p1_id1_c3_t1 [66,72] when (c3_t1 == 1){c3_p1_id1=1; c3_t1=0;}
transition c3_p0_od2_c3_t2 [0,0]  when (c3_p0_od2 == 1 && c2_provide1 >=1){c3_p0_od2=0; c3_t2=1;}
transition c3_p1_id2_c3_t2 [90,99] when (c3_t2 == 1){c3_p1_id2=1; c3_t2=0;}
transition c4_p0_odocks [0,0]  when (c4_p0==1){c4_p0_od0=1; c4_p0_od1=1; c4_p0_od2=1; c4_p0=0;}
transition c4_p1_idocks [0,0]  when (c4_p1_id0==1){c4_p1_id0=0; c4_provide0=1; c4_p1=1;}
transition c4_p2_idocks [0,0]  when (c4_p2_id0==1){c4_p2_id0=0; c4_provide1=1; c4_p2=1;}
transition c4_p3_idocks [0,0]  when (c4_p3_id0==1){c4_p3_id0=0; c4_provide2=1; c4_p3=1;}
transition c4_p0_od0_c4_t0 [0,0]  when (c4_p0_od0 == 1){c4_p0_od0=0; c4_t0=1;}
transition c4_p1_id0_c4_t0 [7,16] when (c4_t0 == 1){c4_p1_id0=1; c4_t0=0;}
transition c4_p0_od1_c4_t1 [0,0]  when (c4_p0_od1 == 1){c4_p0_od1=0; c4_t1=1;}
transition c4_p2_id0_c4_t1 [2,7] when (c4_t1 == 1){c4_p2_id0=1; c4_t1=0;}
transition c4_p0_od2_c4_t2 [0,0]  when (c4_p0_od2 == 1){c4_p0_od2=0; c4_t2=1;}
transition c4_p3_id0_c4_t2 [41,44] when (c4_t2 == 1){c4_p3_id0=1; c4_t2=0;}
check AG (c1_p0==0 || c2_p0==0 || c4_p1==0 || c3_p1==1)
