initially 
{
int badsequence = 0;
int c1_t1_fired = 0;
int c1_t1_neverfired = 1;
int c2_t0_fired = 0;
int c2_t0_neverfired = 1;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p0_od2 = 0;
int c1_p1_id0 = 0;
int c1_p1_id1 = 0;
int c1_p1_id2 = 0;
int c1_p1 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_provide2 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p0_od1 = 0;
int c2_p0_od2 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p2_id0 = 0;
int c2_p2 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0_od2=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1 && c1_p1_id1==1 && c1_p1_id2==1){c1_p1_id0=0; c1_p1_id1=0; c1_p1_id2=0; c1_p1=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c2_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [75,79] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p0_od1_c1_t1 [0,0]  when (c1_p0_od1 == 1 && c2_provide1 >=1){c1_p0_od1=0; c1_t1=1;}
transition c1_p1_id1_c1_t1 [75,75] when (c1_t1 == 1){c1_t1_fired=1; c1_t1_neverfired=0; c1_p1_id1=1; c1_t1=0;}
transition c1_p0_od2_c1_t2 [0,0]  when (c1_p0_od2 == 1 && c2_provide2 >=1){c1_p0_od2=0; c1_t2=1;}
transition c1_p1_id2_c1_t2 [70,70] when (c1_t2 == 1){c1_p1_id2=1; c1_t2=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0_od1=1; c2_p0_od2=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_provide0=1; c2_p1=1;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1){c2_p2_id0=0; c2_provide1=1; c2_p2=1;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_provide2=1; c2_p3=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [85,94] when (c2_t0 == 1){c2_t0_fired=1; c2_t0_neverfired=0; c2_p1_id0=1; c2_t0=0;}
transition c2_p0_od1_c2_t1 [0,0]  when (c2_p0_od1 == 1){c2_p0_od1=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [74,79] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p0_od2_c2_t2 [0,0]  when (c2_p0_od2 == 1){c2_p0_od2=0; c2_t2=1;}
transition c2_p3_id0_c2_t2 [48,50] when (c2_t2 == 1){c2_p3_id0=1; c2_t2=0;}
transition c1_t1_c2_t0_order [0,0]  when (c1_t1_neverfired == 1 && c2_t0_fired == 1){badsequence=1;}
check AG (badsequence==0)
