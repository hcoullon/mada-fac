initially 
{
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p2_id0 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_provide2 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p2_id0 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1){c1_p2_id0=0; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c2_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [89,92] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1 && c2_provide1 >=1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [98,100] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p2_od0_c1_t2 [0,0]  when (c1_p2_od0 == 1 && c2_provide2 >=1){c1_p2_od0=0; c1_t2=1;}
transition c1_p3_id0_c1_t2 [10,10] when (c1_t2 == 1){c1_p3_id0=1; c1_t2=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_provide0=1; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1){c2_p2_id0=0; c2_provide1=1; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_provide2=1; c2_p3=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [70,79] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [58,62] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p2_od0_c2_t2 [0,0]  when (c2_p2_od0 == 1){c2_p2_od0=0; c2_t2=1;}
transition c2_p3_id0_c2_t2 [3,5] when (c2_t2 == 1){c2_p3_id0=1; c2_t2=0;}
cost_rate 1
check[timed_trace, absolute] mincost (c1_p3==1 && c2_p3==1)