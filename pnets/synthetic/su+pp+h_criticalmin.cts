initially 
{
int c1_provide0 = 0;
int c1_provide1 = 0;
int c1_provide2 = 0;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p1_od1 = 0;
int c1_p1_od2 = 0;
int c1_p2_id0 = 0;
int c1_p2_id1 = 0;
int c1_p2_id2 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_p3_od0 = 0;
int c1_p3_od1 = 0;
int c1_p3_od2 = 0;
int c1_p4_id0 = 0;
int c1_p4_id1 = 0;
int c1_p4_id2 = 0;
int c1_p4 = 0;
int c1_p4_od0 = 0;
int c1_p5_id0 = 0;
int c1_p5 = 0;
int c1_p5_od0 = 0;
int c1_p5_od1 = 0;
int c1_p5_od2 = 0;
int c1_p6_id0 = 0;
int c1_p6_id1 = 0;
int c1_p6_id2 = 0;
int c1_p6_id3 = 0;
int c1_p6 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c1_t3 = 0;
int c1_t4 = 0;
int c1_t5 = 0;
int c1_t6 = 0;
int c1_t7 = 0;
int c1_t8 = 0;
int c1_t9 = 0;
int c1_t10 = 0;
int c1_t11 = 0;
int c1_t12 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p2_id0 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
int c3_provide0 = 0;
int c3_provide1 = 0;
int c3_provide2 = 0;
int c3_p0 = 1;
int c3_p0_od0 = 0;
int c3_p0_od1 = 0;
int c3_p0_od2 = 0;
int c3_p1_id0 = 0;
int c3_p1 = 0;
int c3_p2_id0 = 0;
int c3_p2 = 0;
int c3_p3_id0 = 0;
int c3_p3 = 0;
int c3_t0 = 0;
int c3_t1 = 0;
int c3_t2 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1_od1=1; c1_p1_od2=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1 && c1_p2_id1==1 && c1_p2_id2==1){c1_p2_id0=0; c1_p2_id1=0; c1_p2_id2=0; c1_provide0=1; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p3_odocks [0,0]  when (c1_p3==1){c1_p3_od0=1; c1_p3_od1=1; c1_p3_od2=1; c1_p3=0;}
transition c1_p4_idocks [0,0]  when (c1_p4_id0==1 && c1_p4_id1==1 && c1_p4_id2==1){c1_p4_id0=0; c1_p4_id1=0; c1_p4_id2=0; c1_provide1=1; c1_p4=1;}
transition c1_p4_odocks [0,0]  when (c1_p4==1){c1_p4_od0=1; c1_p4=0;}
transition c1_p5_idocks [0,0]  when (c1_p5_id0==1){c1_p5_id0=0; c1_p5=1;}
transition c1_p5_odocks [0,0]  when (c1_p5==1){c1_p5_od0=1; c1_p5_od1=1; c1_p5_od2=1; c1_p5=0;}
transition c1_p6_idocks [0,0]  when (c1_p6_id0==1 && c1_p6_id1==1 && c1_p6_id2==1 && c1_p6_id3==1){c1_p6_id0=0; c1_p6_id1=0; c1_p6_id2=0; c1_p6_id3=0; c1_provide2=1; c1_p6=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c3_provide2 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [33,41] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [19,19] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p1_od1_c1_t2 [0,0]  when (c1_p1_od1 == 1){c1_p1_od1=0; c1_t2=1;}
transition c1_p2_id1_c1_t2 [30,30] when (c1_t2 == 1){c1_p2_id1=1; c1_t2=0;}
transition c1_p1_od2_c1_t3 [0,0]  when (c1_p1_od2 == 1){c1_p1_od2=0; c1_t3=1;}
transition c1_p2_id2_c1_t3 [18,27] when (c1_t3 == 1){c1_p2_id2=1; c1_t3=0;}
transition c1_p2_od0_c1_t4 [0,0]  when (c1_p2_od0 == 1 && c3_provide1 >=1){c1_p2_od0=0; c1_t4=1;}
transition c1_p3_id0_c1_t4 [94,102] when (c1_t4 == 1){c1_p3_id0=1; c1_t4=0;}
transition c1_p3_od0_c1_t5 [0,0]  when (c1_p3_od0 == 1){c1_p3_od0=0; c1_t5=1;}
transition c1_p4_id0_c1_t5 [83,90] when (c1_t5 == 1){c1_p4_id0=1; c1_t5=0;}
transition c1_p3_od1_c1_t6 [0,0]  when (c1_p3_od1 == 1){c1_p3_od1=0; c1_t6=1;}
transition c1_p4_id1_c1_t6 [37,43] when (c1_t6 == 1){c1_p4_id1=1; c1_t6=0;}
transition c1_p3_od2_c1_t7 [0,0]  when (c1_p3_od2 == 1){c1_p3_od2=0; c1_t7=1;}
transition c1_p4_id2_c1_t7 [72,72] when (c1_t7 == 1){c1_p4_id2=1; c1_t7=0;}
transition c1_p4_od0_c1_t8 [0,0]  when (c1_p4_od0 == 1 && c3_provide0 >=1){c1_p4_od0=0; c1_t8=1;}
transition c1_p5_id0_c1_t8 [90,91] when (c1_t8 == 1){c1_p5_id0=1; c1_t8=0;}
transition c1_p5_od0_c1_t9 [0,0]  when (c1_p5_od0 == 1){c1_p5_od0=0; c1_t9=1;}
transition c1_p6_id0_c1_t9 [5,6] when (c1_t9 == 1){c1_p6_id0=1; c1_t9=0;}
transition c1_p5_od1_c1_t10 [0,0]  when (c1_p5_od1 == 1){c1_p5_od1=0; c1_t10=1;}
transition c1_p6_id1_c1_t10 [79,79] when (c1_t10 == 1){c1_p6_id1=1; c1_t10=0;}
transition c1_p5_od2_c1_t11 [0,0]  when (c1_p5_od2 == 1){c1_p5_od2=0; c1_t11=1;}
transition c1_p6_id2_c1_t11 [9,15] when (c1_t11 == 1){c1_p6_id2=1; c1_t11=0;}
transition c1_p0_od1_c1_t12 [0,0]  when (c1_p0_od1 == 1){c1_p0_od1=0; c1_t12=1;}
transition c1_p6_id3_c1_t12 [93,96] when (c1_t12 == 1){c1_p6_id3=1; c1_t12=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1){c2_p2_id0=0; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_p3=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1 && c1_provide0 >=1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [88,91] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1 && c1_provide1 >=1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [13,18] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p2_od0_c2_t2 [0,0]  when (c2_p2_od0 == 1 && c1_provide2 >=1){c2_p2_od0=0; c2_t2=1;}
transition c2_p3_id0_c2_t2 [70,77] when (c2_t2 == 1){c2_p3_id0=1; c2_t2=0;}
transition c3_p0_odocks [0,0]  when (c3_p0==1){c3_p0_od0=1; c3_p0_od1=1; c3_p0_od2=1; c3_p0=0;}
transition c3_p1_idocks [0,0]  when (c3_p1_id0==1){c3_p1_id0=0; c3_provide0=1; c3_p1=1;}
transition c3_p2_idocks [0,0]  when (c3_p2_id0==1){c3_p2_id0=0; c3_provide1=1; c3_p2=1;}
transition c3_p3_idocks [0,0]  when (c3_p3_id0==1){c3_p3_id0=0; c3_provide2=1; c3_p3=1;}
transition c3_p0_od0_c3_t0 [0,0]  when (c3_p0_od0 == 1){c3_p0_od0=0; c3_t0=1;}
transition c3_p1_id0_c3_t0 [45,52] when (c3_t0 == 1){c3_p1_id0=1; c3_t0=0;}
transition c3_p0_od1_c3_t1 [0,0]  when (c3_p0_od1 == 1){c3_p0_od1=0; c3_t1=1;}
transition c3_p2_id0_c3_t1 [17,23] when (c3_t1 == 1){c3_p2_id0=1; c3_t1=0;}
transition c3_p0_od2_c3_t2 [0,0]  when (c3_p0_od2 == 1){c3_p0_od2=0; c3_t2=1;}
transition c3_p3_id0_c3_t2 [72,80] when (c3_t2 == 1){c3_p3_id0=1; c3_t2=0;}
cost_rate 1
check[timed_trace, absolute] mincost (c1_p6==1 && c2_p3==1 && c3_p1==1 && c3_p2==1 && c3_p3==1)