initially 
{
int c1_provide0 = 0;
int c1_provide1 = 0;
int c1_p0 = 1;
int c1_p0_od0 = 0;
int c1_p0_od1 = 0;
int c1_p1_id0 = 0;
int c1_p1 = 0;
int c1_p1_od0 = 0;
int c1_p1_od1 = 0;
int c1_p1_od2 = 0;
int c1_p2_id0 = 0;
int c1_p2_id1 = 0;
int c1_p2_id2 = 0;
int c1_p2 = 0;
int c1_p2_od0 = 0;
int c1_p3_id0 = 0;
int c1_p3 = 0;
int c1_p3_od0 = 0;
int c1_p3_od1 = 0;
int c1_p3_od2 = 0;
int c1_p4_id0 = 0;
int c1_p4_id1 = 0;
int c1_p4_id2 = 0;
int c1_p4_id3 = 0;
int c1_p4 = 0;
int c1_t0 = 0;
int c1_t1 = 0;
int c1_t2 = 0;
int c1_t3 = 0;
int c1_t4 = 0;
int c1_t5 = 0;
int c1_t6 = 0;
int c1_t7 = 0;
int c1_t8 = 0;
int c2_provide0 = 0;
int c2_provide1 = 0;
int c2_p0 = 1;
int c2_p0_od0 = 0;
int c2_p0_od1 = 0;
int c2_p1_id0 = 0;
int c2_p1 = 0;
int c2_p1_od0 = 0;
int c2_p1_od1 = 0;
int c2_p1_od2 = 0;
int c2_p2_id0 = 0;
int c2_p2_id1 = 0;
int c2_p2_id2 = 0;
int c2_p2 = 0;
int c2_p2_od0 = 0;
int c2_p3_id0 = 0;
int c2_p3 = 0;
int c2_p3_od0 = 0;
int c2_p3_od1 = 0;
int c2_p3_od2 = 0;
int c2_p4_id0 = 0;
int c2_p4_id1 = 0;
int c2_p4_id2 = 0;
int c2_p4_id3 = 0;
int c2_p4 = 0;
int c2_t0 = 0;
int c2_t1 = 0;
int c2_t2 = 0;
int c2_t3 = 0;
int c2_t4 = 0;
int c2_t5 = 0;
int c2_t6 = 0;
int c2_t7 = 0;
int c2_t8 = 0;
int c3_p0 = 1;
int c3_p0_od0 = 0;
int c3_p0_od1 = 0;
int c3_p0_od2 = 0;
int c3_p1_id0 = 0;
int c3_p1_id1 = 0;
int c3_p1_id2 = 0;
int c3_p1 = 0;
int c3_t0 = 0;
int c3_t1 = 0;
int c3_t2 = 0;
int c4_provide0 = 0;
int c4_provide1 = 0;
int c4_provide2 = 0;
int c4_p0 = 1;
int c4_p0_od0 = 0;
int c4_p1_id0 = 0;
int c4_p1 = 0;
int c4_p1_od0 = 0;
int c4_p2_id0 = 0;
int c4_p2 = 0;
int c4_p2_od0 = 0;
int c4_p3_id0 = 0;
int c4_p3 = 0;
int c4_t0 = 0;
int c4_t1 = 0;
int c4_t2 = 0;
}
transition c1_p0_odocks [0,0]  when (c1_p0==1){c1_p0_od0=1; c1_p0_od1=1; c1_p0=0;}
transition c1_p1_idocks [0,0]  when (c1_p1_id0==1){c1_p1_id0=0; c1_p1=1;}
transition c1_p1_odocks [0,0]  when (c1_p1==1){c1_p1_od0=1; c1_p1_od1=1; c1_p1_od2=1; c1_p1=0;}
transition c1_p2_idocks [0,0]  when (c1_p2_id0==1 && c1_p2_id1==1 && c1_p2_id2==1){c1_p2_id0=0; c1_p2_id1=0; c1_p2_id2=0; c1_provide0=1; c1_p2=1;}
transition c1_p2_odocks [0,0]  when (c1_p2==1){c1_p2_od0=1; c1_p2=0;}
transition c1_p3_idocks [0,0]  when (c1_p3_id0==1){c1_p3_id0=0; c1_p3=1;}
transition c1_p3_odocks [0,0]  when (c1_p3==1){c1_p3_od0=1; c1_p3_od1=1; c1_p3_od2=1; c1_p3=0;}
transition c1_p4_idocks [0,0]  when (c1_p4_id0==1 && c1_p4_id1==1 && c1_p4_id2==1 && c1_p4_id3==1){c1_p4_id0=0; c1_p4_id1=0; c1_p4_id2=0; c1_p4_id3=0; c1_provide1=1; c1_p4=1;}
transition c1_p0_od0_c1_t0 [0,0]  when (c1_p0_od0 == 1 && c2_provide0 >=1){c1_p0_od0=0; c1_t0=1;}
transition c1_p1_id0_c1_t0 [4,6] when (c1_t0 == 1){c1_p1_id0=1; c1_t0=0;}
transition c1_p1_od0_c1_t1 [0,0]  when (c1_p1_od0 == 1){c1_p1_od0=0; c1_t1=1;}
transition c1_p2_id0_c1_t1 [7,9] when (c1_t1 == 1){c1_p2_id0=1; c1_t1=0;}
transition c1_p1_od1_c1_t2 [0,0]  when (c1_p1_od1 == 1){c1_p1_od1=0; c1_t2=1;}
transition c1_p2_id1_c1_t2 [53,55] when (c1_t2 == 1){c1_p2_id1=1; c1_t2=0;}
transition c1_p1_od2_c1_t3 [0,0]  when (c1_p1_od2 == 1){c1_p1_od2=0; c1_t3=1;}
transition c1_p2_id2_c1_t3 [61,63] when (c1_t3 == 1){c1_p2_id2=1; c1_t3=0;}
transition c1_p2_od0_c1_t4 [0,0]  when (c1_p2_od0 == 1 && c4_provide1 >=1){c1_p2_od0=0; c1_t4=1;}
transition c1_p3_id0_c1_t4 [42,45] when (c1_t4 == 1){c1_p3_id0=1; c1_t4=0;}
transition c1_p3_od0_c1_t5 [0,0]  when (c1_p3_od0 == 1){c1_p3_od0=0; c1_t5=1;}
transition c1_p4_id0_c1_t5 [28,28] when (c1_t5 == 1){c1_p4_id0=1; c1_t5=0;}
transition c1_p3_od1_c1_t6 [0,0]  when (c1_p3_od1 == 1){c1_p3_od1=0; c1_t6=1;}
transition c1_p4_id1_c1_t6 [14,21] when (c1_t6 == 1){c1_p4_id1=1; c1_t6=0;}
transition c1_p3_od2_c1_t7 [0,0]  when (c1_p3_od2 == 1){c1_p3_od2=0; c1_t7=1;}
transition c1_p4_id2_c1_t7 [94,103] when (c1_t7 == 1){c1_p4_id2=1; c1_t7=0;}
transition c1_p0_od1_c1_t8 [0,0]  when (c1_p0_od1 == 1){c1_p0_od1=0; c1_t8=1;}
transition c1_p4_id3_c1_t8 [99,106] when (c1_t8 == 1){c1_p4_id3=1; c1_t8=0;}
transition c2_p0_odocks [0,0]  when (c2_p0==1){c2_p0_od0=1; c2_p0_od1=1; c2_p0=0;}
transition c2_p1_idocks [0,0]  when (c2_p1_id0==1){c2_p1_id0=0; c2_p1=1;}
transition c2_p1_odocks [0,0]  when (c2_p1==1){c2_p1_od0=1; c2_p1_od1=1; c2_p1_od2=1; c2_p1=0;}
transition c2_p2_idocks [0,0]  when (c2_p2_id0==1 && c2_p2_id1==1 && c2_p2_id2==1){c2_p2_id0=0; c2_p2_id1=0; c2_p2_id2=0; c2_provide0=1; c2_p2=1;}
transition c2_p2_odocks [0,0]  when (c2_p2==1){c2_p2_od0=1; c2_p2=0;}
transition c2_p3_idocks [0,0]  when (c2_p3_id0==1){c2_p3_id0=0; c2_p3=1;}
transition c2_p3_odocks [0,0]  when (c2_p3==1){c2_p3_od0=1; c2_p3_od1=1; c2_p3_od2=1; c2_p3=0;}
transition c2_p4_idocks [0,0]  when (c2_p4_id0==1 && c2_p4_id1==1 && c2_p4_id2==1 && c2_p4_id3==1){c2_p4_id0=0; c2_p4_id1=0; c2_p4_id2=0; c2_p4_id3=0; c2_provide1=1; c2_p4=1;}
transition c2_p0_od0_c2_t0 [0,0]  when (c2_p0_od0 == 1 && c4_provide0 >=1){c2_p0_od0=0; c2_t0=1;}
transition c2_p1_id0_c2_t0 [10,12] when (c2_t0 == 1){c2_p1_id0=1; c2_t0=0;}
transition c2_p1_od0_c2_t1 [0,0]  when (c2_p1_od0 == 1){c2_p1_od0=0; c2_t1=1;}
transition c2_p2_id0_c2_t1 [77,82] when (c2_t1 == 1){c2_p2_id0=1; c2_t1=0;}
transition c2_p1_od1_c2_t2 [0,0]  when (c2_p1_od1 == 1){c2_p1_od1=0; c2_t2=1;}
transition c2_p2_id1_c2_t2 [24,26] when (c2_t2 == 1){c2_p2_id1=1; c2_t2=0;}
transition c2_p1_od2_c2_t3 [0,0]  when (c2_p1_od2 == 1){c2_p1_od2=0; c2_t3=1;}
transition c2_p2_id2_c2_t3 [60,69] when (c2_t3 == 1){c2_p2_id2=1; c2_t3=0;}
transition c2_p2_od0_c2_t4 [0,0]  when (c2_p2_od0 == 1 && c4_provide2 >=1){c2_p2_od0=0; c2_t4=1;}
transition c2_p3_id0_c2_t4 [93,93] when (c2_t4 == 1){c2_p3_id0=1; c2_t4=0;}
transition c2_p3_od0_c2_t5 [0,0]  when (c2_p3_od0 == 1){c2_p3_od0=0; c2_t5=1;}
transition c2_p4_id0_c2_t5 [82,87] when (c2_t5 == 1){c2_p4_id0=1; c2_t5=0;}
transition c2_p3_od1_c2_t6 [0,0]  when (c2_p3_od1 == 1){c2_p3_od1=0; c2_t6=1;}
transition c2_p4_id1_c2_t6 [18,18] when (c2_t6 == 1){c2_p4_id1=1; c2_t6=0;}
transition c2_p3_od2_c2_t7 [0,0]  when (c2_p3_od2 == 1){c2_p3_od2=0; c2_t7=1;}
transition c2_p4_id2_c2_t7 [59,67] when (c2_t7 == 1){c2_p4_id2=1; c2_t7=0;}
transition c2_p0_od1_c2_t8 [0,0]  when (c2_p0_od1 == 1){c2_p0_od1=0; c2_t8=1;}
transition c2_p4_id3_c2_t8 [67,73] when (c2_t8 == 1){c2_p4_id3=1; c2_t8=0;}
transition c3_p0_odocks [0,0]  when (c3_p0==1){c3_p0_od0=1; c3_p0_od1=1; c3_p0_od2=1; c3_p0=0;}
transition c3_p1_idocks [0,0]  when (c3_p1_id0==1 && c3_p1_id1==1 && c3_p1_id2==1){c3_p1_id0=0; c3_p1_id1=0; c3_p1_id2=0; c3_p1=1;}
transition c3_p0_od0_c3_t0 [0,0]  when (c3_p0_od0 == 1 && c1_provide0 >=1){c3_p0_od0=0; c3_t0=1;}
transition c3_p1_id0_c3_t0 [99,105] when (c3_t0 == 1){c3_p1_id0=1; c3_t0=0;}
transition c3_p0_od1_c3_t1 [0,0]  when (c3_p0_od1 == 1 && c1_provide1 >=1){c3_p0_od1=0; c3_t1=1;}
transition c3_p1_id1_c3_t1 [29,32] when (c3_t1 == 1){c3_p1_id1=1; c3_t1=0;}
transition c3_p0_od2_c3_t2 [0,0]  when (c3_p0_od2 == 1 && c2_provide1 >=1){c3_p0_od2=0; c3_t2=1;}
transition c3_p1_id2_c3_t2 [79,86] when (c3_t2 == 1){c3_p1_id2=1; c3_t2=0;}
transition c4_p0_odocks [0,0]  when (c4_p0==1){c4_p0_od0=1; c4_p0=0;}
transition c4_p1_idocks [0,0]  when (c4_p1_id0==1){c4_p1_id0=0; c4_provide0=1; c4_p1=1;}
transition c4_p1_odocks [0,0]  when (c4_p1==1){c4_p1_od0=1; c4_p1=0;}
transition c4_p2_idocks [0,0]  when (c4_p2_id0==1){c4_p2_id0=0; c4_provide1=1; c4_p2=1;}
transition c4_p2_odocks [0,0]  when (c4_p2==1){c4_p2_od0=1; c4_p2=0;}
transition c4_p3_idocks [0,0]  when (c4_p3_id0==1){c4_p3_id0=0; c4_provide2=1; c4_p3=1;}
transition c4_p0_od0_c4_t0 [0,0]  when (c4_p0_od0 == 1){c4_p0_od0=0; c4_t0=1;}
transition c4_p1_id0_c4_t0 [44,50] when (c4_t0 == 1){c4_p1_id0=1; c4_t0=0;}
transition c4_p1_od0_c4_t1 [0,0]  when (c4_p1_od0 == 1){c4_p1_od0=0; c4_t1=1;}
transition c4_p2_id0_c4_t1 [13,19] when (c4_t1 == 1){c4_p2_id0=1; c4_t1=0;}
transition c4_p2_od0_c4_t2 [0,0]  when (c4_p2_od0 == 1){c4_p2_od0=0; c4_t2=1;}
transition c4_p3_id0_c4_t2 [97,104] when (c4_t2 == 1){c4_p3_id0=1; c4_t2=0;}
check AG (c3_p0==0 || c1_p0==1 || c2_p3==1 || c4_p3==1)
