initially 
{
int facts_facts = 1;
int facts_deployed = 1;
int common_common = 0;
int common_initiated = 1;
int common_initiated_od0 = 0;
int common_deployed_id0 = 0;
int common_deployed = 0;
int common_ktb_deployed_id0 = 0;
int common_ktb_deployed = 0;
int common_ktb_deployed_od0 = 0;
int common_ktb_deploy = 0;
int common_deploy = 0;
int haproxy_haproxy = 0;
int haproxy_initiated = 1;
int haproxy_initiated_od0 = 0;
int haproxy_deployed_id0 = 0;
int haproxy_deployed = 0;
int haproxy_deploy = 0;
int memcached_initiated = 1;
int memcached_initiated_od0 = 0;
int memcached_deployed_id0 = 0;
int memcached_deployed = 0;
int memcached_deploy = 0;
int rabbitmq_rabd = 1;
int rabbitmq_initiated = 1;
int rabbitmq_initiated_od0 = 0;
int rabbitmq_deployed_id0 = 0;
int rabbitmq_deployed = 0;
int rabbitmq_deploy = 0;
int mariadb_mdbd = 1;
int mariadb_mariadb = 0;
int mariadb_initiated = 1;
int mariadb_initiated_od0 = 0;
int mariadb_pulled_id0 = 0;
int mariadb_pulled = 0;
int mariadb_pulled_od0 = 0;
int mariadb_deployed_id0 = 0;
int mariadb_deployed = 0;
int mariadb_pull = 0;
int mariadb_deploy = 0;
int keystone_kstd = 1;
int keystone_keystone = 0;
int keystone_initiated = 1;
int keystone_initiated_od0 = 0;
int keystone_pulled_id0 = 0;
int keystone_pulled = 0;
int keystone_pulled_od0 = 0;
int keystone_deployed_id0 = 0;
int keystone_deployed = 0;
int keystone_pull = 0;
int keystone_deploy = 0;
int glance_glad = 1;
int glance_initiated = 1;
int glance_initiated_od0 = 0;
int glance_initiated_od1 = 0;
int glance_initiated_od2 = 0;
int glance_pulled_id0 = 0;
int glance_pulled_id1 = 0;
int glance_pulled_id2 = 0;
int glance_pulled = 0;
int glance_pulled_od0 = 0;
int glance_deployed_id0 = 0;
int glance_deployed = 0;
int glance_pull = 0;
int glance_config = 0;
int glance_register = 0;
int glance_deploy = 0;
int nova_novad = 1;
int nova_initiated = 1;
int nova_initiated_od0 = 0;
int nova_initiated_od1 = 0;
int nova_initiated_od2 = 0;
int nova_pulled_id0 = 0;
int nova_pulled_id1 = 0;
int nova_pulled_id2 = 0;
int nova_pulled = 0;
int nova_pulled_od0 = 0;
int nova_deployed_id0 = 0;
int nova_deployed = 0;
int nova_pull = 0;
int nova_config = 0;
int nova_register = 0;
int nova_deploy = 0;
int openvswitch_initiated = 1;
int openvswitch_initiated_od0 = 0;
int openvswitch_deployed_id0 = 0;
int openvswitch_deployed = 0;
int openvswitch_deploy = 0;
int neutron_neud = 1;
int neutron_initiated = 1;
int neutron_initiated_od0 = 0;
int neutron_initiated_od1 = 0;
int neutron_initiated_od2 = 0;
int neutron_pulled_id0 = 0;
int neutron_pulled_id1 = 0;
int neutron_pulled_id2 = 0;
int neutron_pulled = 0;
int neutron_pulled_od0 = 0;
int neutron_deployed_id0 = 0;
int neutron_deployed = 0;
int neutron_pull = 0;
int neutron_config = 0;
int neutron_register = 0;
int neutron_deploy = 0;
}
transition common_initiated_odocks [0,0]  when (common_initiated==1){common_initiated_od0=1; common_initiated=0;}
transition common_deployed_idocks [0,0]  when (common_deployed_id0==1){common_deployed_id0=0; common_deployed=1;}
transition common_ktb_deployed_idocks [0,0]  when (common_ktb_deployed_id0==1){common_ktb_deployed_id0=0; common_common=1; common_ktb_deployed=1;}
transition common_ktb_deployed_odocks [0,0]  when (common_ktb_deployed==1){common_ktb_deployed_od0=1; common_ktb_deployed=0;}
transition common_initiated_od0_common_ktb_deploy [0,0]  when (common_initiated_od0 == 1 && facts_facts >=1){common_initiated_od0=0; common_ktb_deploy=1;}
transition common_ktb_deployed_id0_common_ktb_deploy [6,7] when (common_ktb_deploy == 1){common_ktb_deployed_id0=1; common_ktb_deploy=0;}
transition common_ktb_deployed_od0_common_deploy [0,0]  when (common_ktb_deployed_od0 == 1){common_ktb_deployed_od0=0; common_deploy=1;}
transition common_deployed_id0_common_deploy [25,26] when (common_deploy == 1){common_deployed_id0=1; common_deploy=0;}
transition haproxy_initiated_odocks [0,0]  when (haproxy_initiated==1){haproxy_initiated_od0=1; haproxy_initiated=0;}
transition haproxy_deployed_idocks [0,0]  when (haproxy_deployed_id0==1){haproxy_deployed_id0=0; haproxy_haproxy=1; haproxy_deployed=1;}
transition haproxy_initiated_od0_haproxy_deploy [0,0]  when (haproxy_initiated_od0 == 1 && facts_facts >=1 && mariadb_mdbd >=1 && keystone_kstd >=1 && nova_novad >=1 && glance_glad >=1 && neutron_neud >=1){haproxy_initiated_od0=0; haproxy_deploy=1;}
transition haproxy_deployed_id0_haproxy_deploy [19,20] when (haproxy_deploy == 1){haproxy_deployed_id0=1; haproxy_deploy=0;}
transition memcached_initiated_odocks [0,0]  when (memcached_initiated==1){memcached_initiated_od0=1; memcached_initiated=0;}
transition memcached_deployed_idocks [0,0]  when (memcached_deployed_id0==1){memcached_deployed_id0=0; memcached_deployed=1;}
transition memcached_initiated_od0_memcached_deploy [0,0]  when (memcached_initiated_od0 == 1 && facts_facts >=1){memcached_initiated_od0=0; memcached_deploy=1;}
transition memcached_deployed_id0_memcached_deploy [4,5] when (memcached_deploy == 1){memcached_deployed_id0=1; memcached_deploy=0;}
transition rabbitmq_initiated_odocks [0,0]  when (rabbitmq_initiated==1){rabbitmq_initiated_od0=1; rabbitmq_initiated=0;}
transition rabbitmq_deployed_idocks [0,0]  when (rabbitmq_deployed_id0==1){rabbitmq_deployed_id0=0; rabbitmq_deployed=1;}
transition rabbitmq_initiated_od0_rabbitmq_deploy [0,0]  when (rabbitmq_initiated_od0 == 1 && facts_facts >=1){rabbitmq_initiated_od0=0; rabbitmq_deploy=1;}
transition rabbitmq_deployed_id0_rabbitmq_deploy [9,10] when (rabbitmq_deploy == 1){rabbitmq_deployed_id0=1; rabbitmq_deploy=0;}
transition mariadb_initiated_odocks [0,0]  when (mariadb_initiated==1){mariadb_initiated_od0=1; mariadb_initiated=0;}
transition mariadb_pulled_idocks [0,0]  when (mariadb_pulled_id0==1){mariadb_pulled_id0=0; mariadb_pulled=1;}
transition mariadb_pulled_odocks [0,0]  when (mariadb_pulled==1){mariadb_pulled_od0=1; mariadb_pulled=0;}
transition mariadb_deployed_idocks [0,0]  when (mariadb_deployed_id0==1){mariadb_deployed_id0=0; mariadb_mariadb=1; mariadb_deployed=1;}
transition mariadb_initiated_od0_mariadb_pull [0,0]  when (mariadb_initiated_od0 == 1){mariadb_initiated_od0=0; mariadb_pull=1;}
transition mariadb_pulled_id0_mariadb_pull [4,5] when (mariadb_pull == 1){mariadb_pulled_id0=1; mariadb_pull=0;}
transition mariadb_pulled_od0_mariadb_deploy [0,0]  when (mariadb_pulled_od0 == 1 && common_common >=1 && haproxy_haproxy >=1){mariadb_pulled_od0=0; mariadb_deploy=1;}
transition mariadb_deployed_id0_mariadb_deploy [62,67] when (mariadb_deploy == 1){mariadb_deployed_id0=1; mariadb_deploy=0;}
transition keystone_initiated_odocks [0,0]  when (keystone_initiated==1){keystone_initiated_od0=1; keystone_initiated=0;}
transition keystone_pulled_idocks [0,0]  when (keystone_pulled_id0==1){keystone_pulled_id0=0; keystone_pulled=1;}
transition keystone_pulled_odocks [0,0]  when (keystone_pulled==1){keystone_pulled_od0=1; keystone_pulled=0;}
transition keystone_deployed_idocks [0,0]  when (keystone_deployed_id0==1){keystone_deployed_id0=0; keystone_keystone=1; keystone_deployed=1;}
transition keystone_initiated_od0_keystone_pull [0,0]  when (keystone_initiated_od0 == 1){keystone_initiated_od0=0; keystone_pull=1;}
transition keystone_pulled_id0_keystone_pull [4,5] when (keystone_pull == 1){keystone_pulled_id0=1; keystone_pull=0;}
transition keystone_pulled_od0_keystone_deploy [0,0]  when (keystone_pulled_od0 == 1 && mariadb_mariadb >=1 && mariadb_mdbd >=1){keystone_pulled_od0=0; keystone_deploy=1;}
transition keystone_deployed_id0_keystone_deploy [98,106] when (keystone_deploy == 1){keystone_deployed_id0=1; keystone_deploy=0;}
transition glance_initiated_odocks [0,0]  when (glance_initiated==1){glance_initiated_od0=1; glance_initiated_od1=1; glance_initiated_od2=1; glance_initiated=0;}
transition glance_pulled_idocks [0,0]  when (glance_pulled_id0==1 && glance_pulled_id1==1 && glance_pulled_id2==1){glance_pulled_id0=0; glance_pulled_id1=0; glance_pulled_id2=0; glance_pulled=1;}
transition glance_pulled_odocks [0,0]  when (glance_pulled==1){glance_pulled_od0=1; glance_pulled=0;}
transition glance_deployed_idocks [0,0]  when (glance_deployed_id0==1){glance_deployed_id0=0; glance_deployed=1;}
transition glance_initiated_od0_glance_pull [0,0]  when (glance_initiated_od0 == 1){glance_initiated_od0=0; glance_pull=1;}
transition glance_pulled_id0_glance_pull [5,6] when (glance_pull == 1){glance_pulled_id0=1; glance_pull=0;}
transition glance_initiated_od1_glance_config [0,0]  when (glance_initiated_od1 == 1 && mariadb_mariadb >=1 && mariadb_mdbd >=1 && keystone_kstd >=1 && rabbitmq_rabd >=1){glance_initiated_od1=0; glance_config=1;}
transition glance_pulled_id1_glance_config [8,9] when (glance_config == 1){glance_pulled_id1=1; glance_config=0;}
transition glance_initiated_od2_glance_register [0,0]  when (glance_initiated_od2 == 1 && keystone_kstd >=1 && keystone_keystone >=1){glance_initiated_od2=0; glance_register=1;}
transition glance_pulled_id2_glance_register [24,27] when (glance_register == 1){glance_pulled_id2=1; glance_register=0;}
transition glance_pulled_od0_glance_deploy [0,0]  when (glance_pulled_od0 == 1){glance_pulled_od0=0; glance_deploy=1;}
transition glance_deployed_id0_glance_deploy [91,98] when (glance_deploy == 1){glance_deployed_id0=1; glance_deploy=0;}
transition nova_initiated_odocks [0,0]  when (nova_initiated==1){nova_initiated_od0=1; nova_initiated_od1=1; nova_initiated_od2=1; nova_initiated=0;}
transition nova_pulled_idocks [0,0]  when (nova_pulled_id0==1 && nova_pulled_id1==1 && nova_pulled_id2==1){nova_pulled_id0=0; nova_pulled_id1=0; nova_pulled_id2=0; nova_pulled=1;}
transition nova_pulled_odocks [0,0]  when (nova_pulled==1){nova_pulled_od0=1; nova_pulled=0;}
transition nova_deployed_idocks [0,0]  when (nova_deployed_id0==1){nova_deployed_id0=0; nova_deployed=1;}
transition nova_initiated_od0_nova_pull [0,0]  when (nova_initiated_od0 == 1){nova_initiated_od0=0; nova_pull=1;}
transition nova_pulled_id0_nova_pull [8,9] when (nova_pull == 1){nova_pulled_id0=1; nova_pull=0;}
transition nova_initiated_od1_nova_config [0,0]  when (nova_initiated_od1 == 1 && mariadb_mariadb >=1 && mariadb_mdbd >=1 && keystone_kstd >=1 && rabbitmq_rabd >=1 && glance_glad >=1){nova_initiated_od1=0; nova_config=1;}
transition nova_pulled_id1_nova_config [20,23] when (nova_config == 1){nova_pulled_id1=1; nova_config=0;}
transition nova_initiated_od2_nova_register [0,0]  when (nova_initiated_od2 == 1 && keystone_kstd >=1 && keystone_keystone >=1){nova_initiated_od2=0; nova_register=1;}
transition nova_pulled_id2_nova_register [51,58] when (nova_register == 1){nova_pulled_id2=1; nova_register=0;}
transition nova_pulled_od0_nova_deploy [0,0]  when (nova_pulled_od0 == 1){nova_pulled_od0=0; nova_deploy=1;}
transition nova_deployed_id0_nova_deploy [345,364] when (nova_deploy == 1){nova_deployed_id0=1; nova_deploy=0;}
transition openvswitch_initiated_odocks [0,0]  when (openvswitch_initiated==1){openvswitch_initiated_od0=1; openvswitch_initiated=0;}
transition openvswitch_deployed_idocks [0,0]  when (openvswitch_deployed_id0==1){openvswitch_deployed_id0=0; openvswitch_deployed=1;}
transition openvswitch_initiated_od0_openvswitch_deploy [0,0]  when (openvswitch_initiated_od0 == 1 && facts_facts >=1){openvswitch_initiated_od0=0; openvswitch_deploy=1;}
transition openvswitch_deployed_id0_openvswitch_deploy [10,12] when (openvswitch_deploy == 1){openvswitch_deployed_id0=1; openvswitch_deploy=0;}
transition neutron_initiated_odocks [0,0]  when (neutron_initiated==1){neutron_initiated_od0=1; neutron_initiated_od1=1; neutron_initiated_od2=1; neutron_initiated=0;}
transition neutron_pulled_idocks [0,0]  when (neutron_pulled_id0==1 && neutron_pulled_id1==1 && neutron_pulled_id2==1){neutron_pulled_id0=0; neutron_pulled_id1=0; neutron_pulled_id2=0; neutron_pulled=1;}
transition neutron_pulled_odocks [0,0]  when (neutron_pulled==1){neutron_pulled_od0=1; neutron_pulled=0;}
transition neutron_deployed_idocks [0,0]  when (neutron_deployed_id0==1){neutron_deployed_id0=0; neutron_deployed=1;}
transition neutron_initiated_od0_neutron_pull [0,0]  when (neutron_initiated_od0 == 1){neutron_initiated_od0=0; neutron_pull=1;}
transition neutron_pulled_id0_neutron_pull [7,8] when (neutron_pull == 1){neutron_pulled_id0=1; neutron_pull=0;}
transition neutron_initiated_od1_neutron_config [0,0]  when (neutron_initiated_od1 == 1 && mariadb_mariadb >=1 && mariadb_mdbd >=1 && keystone_kstd >=1 && rabbitmq_rabd >=1){neutron_initiated_od1=0; neutron_config=1;}
transition neutron_pulled_id1_neutron_config [26,29] when (neutron_config == 1){neutron_pulled_id1=1; neutron_config=0;}
transition neutron_initiated_od2_neutron_register [0,0]  when (neutron_initiated_od2 == 1 && keystone_kstd >=1 && keystone_keystone >=1){neutron_initiated_od2=0; neutron_register=1;}
transition neutron_pulled_id2_neutron_register [21,27] when (neutron_register == 1){neutron_pulled_id2=1; neutron_register=0;}
transition neutron_pulled_od0_neutron_deploy [0,0]  when (neutron_pulled_od0 == 1){neutron_pulled_od0=0; neutron_deploy=1;}
transition neutron_deployed_id0_neutron_deploy [200,203] when (neutron_deploy == 1){neutron_deployed_id0=1; neutron_deploy=0;}

check AF (nova_deployed>0 && keystone_deployed>0 && neutron_deployed>0 && glance_deployed>0 && mariadb_deployed>0)

check maxv (nova_pull + nova_config + nova_register + nova_deploy)

check maxv (nova_pull + nova_config + nova_register + nova_deploy + common_ktb_deploy + common_deploy + haproxy_deploy + memcached_deploy + rabbitmq_deploy + mariadb_pull + mariadb_deploy + keystone_pull + keystone_deploy + glance_pull + glance_config + glance_register + glance_deploy + openvswitch_deploy + neutron_pull + neutron_config + neutron_register + neutron_deploy)

//cost_rate 1
//check[timed_trace,absolute] mincost (nova_deployed>0 && keystone_deployed>0 && neutron_deployed>0 && glance_deployed>0 && mariadb_deployed>0)

//cost_rate -1
//check[timed_trace,absolute,neg_costs] mincost (nova_deployed>0 && keystone_deployed>0 && neutron_deployed>0 && glance_deployed>0 && mariadb_deployed>0)
