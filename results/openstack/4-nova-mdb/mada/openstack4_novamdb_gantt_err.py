import sys
sys.path.insert(0, '../../')
from gnuplot_gantt import *

tasks = [
    ("glance deploy", 129,261),
    ("neutron deploy",101,261),
    ("nova deploy",101,261),
    ("nova register",92,129),
    ("glance register",92,129),
    ("neutron register",92,101),
    ("nova upg-db",37,101),
    ("nova upg-api-db",37,101),
    ("kst deploy",31,92),
    ("neutron config",31,44),
    ("nova create-db",31,37),
    ("glance config",31,37),
    ("mariadb check",24,31),
    ("mariadb register",24,26),
    ("mariadb restart",16,24),
    ("com deploy",3,37),
    ("mariadb bootstrap",0,16),
    ("nova config",0,24),
    ("haproxy deploy",0,24),
    ("rabmq deploy",0,4),
    ("ovs deploy",0,16),
    ("nova pull",0,4),
    ("neutron pull",0,4),
    ("com ktb-deploy",0,3),
    ("glance pull",0,2),
    ("memc deploy",0,2),
    ("mariadb pull",0,2),
    ("kst pull",0,2)]

if __name__ == '__main__':
    gnuplot_file_from_list(tasks, "openstack4_novamdb_gantt_err.gnu", title='')
