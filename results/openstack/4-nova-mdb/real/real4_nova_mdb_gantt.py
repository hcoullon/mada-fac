import sys
sys.path.insert(0, '../../')
from gnuplot_gantt import *

tasks = [
    ("glance deploy", 183,295),
    ("neutron deploy",184,387),
    ("nova deploy",171,364),
    ("nova register",156,210),
    ("glance register",152,176),
    ("neutron register",156,183),
    ("nova upg-db",72,171),
    ("nova upg-api-db",72,116),
    ("kst deploy",56,156),
    ("neutron config",56,85),
    ("nova create-db",56,71),
    ("glance config",56,64),
    ("mariadb check",43,56),
    ("mariadb register",43,47),
    ("mariadb restart",28,43),
    ("com deploy",7,33),
    ("mariadb bootstrap",0,32),
    ("nova config",0,23),
    ("haproxy deploy",0,20),
    ("rabmq deploy",0,10),
    ("ovs deploy",1,13),
    ("nova pull",0,9),
    ("neutron pull",0,7),
    ("com ktb-deploy",0,7),
    ("glance pull",0,4),
    ("memc deploy",0,4),
    ("mariadb pull",0,4),
    ("kst pull",0,4)]

if __name__ == '__main__':
    gnuplot_file_from_list(tasks, "real4_novamdb_gantt.gnu", title='')
