- {Component: MariaDB, ElapsedTime: 4.0, Finish: '2018-02-20 02:51:47', Result: Complete,
  Start: '2018-02-20 02:51:43', Task: MariaDB pull}
- {Component: Keystone, ElapsedTime: 4.0, Finish: '2018-02-20 02:51:47', Result: Complete,
  Start: '2018-02-20 02:51:43', Task: Keystone pull}
- {Component: Facts, ElapsedTime: 4.0, Finish: '2018-02-20 02:51:47', Result: Complete,
  Start: '2018-02-20 02:51:43', Task: Facts deploy}
- {Component: Glance, ElapsedTime: 6.0, Finish: '2018-02-20 02:51:49', Result: Complete,
  Start: '2018-02-20 02:51:43', Task: Glance pull}
- {Component: MemCached, ElapsedTime: 5.0, Finish: '2018-02-20 02:51:52',
  Result: Complete, Start: '2018-02-20 02:51:47', Task: MemCached deploy}
- {Component: Neutron, ElapsedTime: 10.0, Finish: '2018-02-20 02:51:53', Result: Complete,
  Start: '2018-02-20 02:51:43', Task: Neutron pull}
- {Component: Common, ElapsedTime: 7.0, Finish: '2018-02-20 02:51:54', Result: Complete,
  Start: '2018-02-20 02:51:47', Task: Common ktb_deploy}
- {Component: RabbitMQ, ElapsedTime: 10.0, Finish: '2018-02-20 02:51:57',
  Result: Complete, Start: '2018-02-20 02:51:47', Task: RabbitMQ deploy}
- {Component: Nova, ElapsedTime: 15.0, Finish: '2018-02-20 02:51:58', Result: Complete,
  Start: '2018-02-20 02:51:43', Task: Nova pull}
- {Component: OpenVSwitch, ElapsedTime: 11.0, Finish: '2018-02-20 02:51:58',
  Result: Complete, Start: '2018-02-20 02:51:47', Task: OpenVSwitch deploy}
- {Component: Nova, ElapsedTime: 22.0, Finish: '2018-02-20 02:52:05', Result: Complete,
  Start: '2018-02-20 02:51:43', Task: Nova config}
- {Component: HAProxy, ElapsedTime: 19.0, Finish: '2018-02-20 02:52:06', Result: Complete,
  Start: '2018-02-20 02:51:47', Task: HAProxy deploy}
- {Component: Common, ElapsedTime: 24.0, Finish: '2018-02-20 02:52:18', Result: Complete,
  Start: '2018-02-20 02:51:54', Task: Common deploy}
- {Component: MariaDB, ElapsedTime: 77.0, Finish: '2018-02-20 02:53:23', Result: Complete,
  Start: '2018-02-20 02:52:06', Task: MariaDB deploy}
- {Component: Glance, ElapsedTime: 8.0, Finish: '2018-02-20 02:53:31', Result: Complete,
  Start: '2018-02-20 02:53:23', Task: Glance config}
- {Component: Nova, ElapsedTime: 15.0, Finish: '2018-02-20 02:53:38', Result: Complete,
  Start: '2018-02-20 02:53:23', Task: Nova create_db}
- {Component: Neutron, ElapsedTime: 26.0, Finish: '2018-02-20 02:53:49', Result: Complete,
  Start: '2018-02-20 02:53:23', Task: Neutron config}
- {Component: Keystone, ElapsedTime: 65.0, Finish: '2018-02-20 02:54:28',
  Result: Complete, Start: '2018-02-20 02:53:23', Task: Keystone deploy}
- {Component: Glance, ElapsedTime: 27.0, Finish: '2018-02-20 02:54:55', Result: Complete,
  Start: '2018-02-20 02:54:28', Task: Glance register}
- {Component: Neutron, ElapsedTime: 27.0, Finish: '2018-02-20 02:54:55', Result: Complete,
  Start: '2018-02-20 02:54:28', Task: Neutron register}
- {Component: Nova, ElapsedTime: 56.0, Finish: '2018-02-20 02:55:24', Result: Complete,
  Start: '2018-02-20 02:54:28', Task: Nova register}
- {Component: Nova, ElapsedTime: 53.0, Finish: '2018-02-20 02:56:17', Result: Complete,
  Start: '2018-02-20 02:55:24', Task: Nova upgrade_api_db}
- {Component: Glance, ElapsedTime: 95.0, Finish: '2018-02-20 02:56:30', Result: Complete,
  Start: '2018-02-20 02:54:55', Task: Glance deploy}
- {Component: Nova, ElapsedTime: 154.0, Finish: '2018-02-20 02:57:58', Result: Complete,
  Start: '2018-02-20 02:55:24', Task: Nova upgrade_db}
- {Component: Nova, ElapsedTime: 14.0, Finish: '2018-02-20 02:58:12', Result: Complete,
  Start: '2018-02-20 02:57:58', Task: Nova restart}
- {Component: Neutron, ElapsedTime: 213.0, Finish: '2018-02-20 02:58:28',
  Result: Complete, Start: '2018-02-20 02:54:55', Task: Neutron deploy}
- {Component: Nova, ElapsedTime: 107.0, Finish: '2018-02-20 02:59:59', Result: Complete,
  Start: '2018-02-20 02:58:12', Task: Nova simple_cell_setup}
