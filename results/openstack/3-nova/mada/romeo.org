* deployability
[info] Checking A (true U ((((( nova_deployed == 1 ) and ( keystone_deployed == 1 )) and ( neutron_deployed == 1 )) and ( glance_deployed == 1 )) and ( mariadb_deployed == 1 )))
[info] Computation mode is time
[info] Computation method is state classes.
[info] Convergence method is merge on common markings.
true

No trace.
[info] Time: 73.1s (total) = 69.2s (user) + 3.9s (system)
[info] Max memory used: 479.9Mo
* parallelism on Nova component
[info] Checking when true maximize ( ( ( ( ( ( nova_pull + nova_config ) + nova_create_db ) + nova_register ) + nova_upgrade_db ) + nova_upgrade_api_db ) + nova_deploy )
[info] Computation mode is time
[info] Computation method is state classes.
[info] Convergence method is merge on common markings.
2

Trace: neutron_initiated_odocks, neutron_initiated_od0_neutron_pull, openvswitch_initiated_odocks, openvswitch_initiated_od0_openvswitch_deploy, nova_initiated_odocks, nova_initiated_od1_nova_config, nova_initiated_od0_nova_pull
[info] Time: 74.3s (total) = 69.8s (user) + 4.5s (system)
[info] Max memory used: 1389.0Mo
* parallelism on full assembly
[info] Checking when true maximize ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( common_ktb_deploy + common_deploy ) + haproxy_deploy ) + memcached_deploy ) + rabbitmq_deploy ) + mariadb_pull ) + mariadb_deploy ) + keystone_pull ) + keystone_deploy ) + glance_pull ) + glance_config ) + glance_register ) + glance_deploy ) + nova_pull ) + nova_config ) + nova_create_db ) + nova_register ) + nova_upgrade_db ) + nova_upgrade_api_db ) + nova_deploy ) + openvswitch_deploy ) + neutron_pull ) + neutron_config ) + neutron_register ) + neutron_deploy )
[info] Computation mode is time
[info] Computation method is state classes.
[info] Convergence method is merge on common markings.
11

Trace: neutron_initiated_odocks, neutron_initiated_od0_neutron_pull, openvswitch_initiated_odocks, openvswitch_initiated_od0_openvswitch_deploy, nova_initiated_odocks, nova_initiated_od1_nova_config, nova_initiated_od0_nova_pull, glance_initiated_odocks, glance_initiated_od0_glance_pull, keystone_initiated_odocks, keystone_initiated_od0_keystone_pull, mariadb_initiated_odocks, mariadb_initiated_od0_mariadb_pull, rabbitmq_initiated_odocks, rabbitmq_initiated_od0_rabbitmq_deploy, memcached_initiated_odocks, memcached_initiated_od0_memcached_deploy, haproxy_initiated_odocks, haproxy_initiated_od0_haproxy_deploy, common_initiated_odocks, common_initiated_od0_common_ktb_deploy
[info] Time: 76.9s (total) = 72.6s (user) + 4.3s (system)
[info] Max memory used: 1392.0Mo
* gantt boundaries min
[info] Checking mincost E (true U ((((( nova_deployed > 0 ) and ( keystone_deployed > 0 )) and ( neutron_deployed > 0 )) and ( glance_deployed > 0 )) and ( mariadb_deployed > 0 )))
[info] Computation mode is time
[info] Infimum cost computation uses simple cost state classes.
[info] Convergence method is merge on common markings.
=400

Trace: neutron_initiated_odocks@0, neutron_initiated_od0_neutron_pull@0, openvswitch_initiated_odocks@0, openvswitch_initiated_od0_openvswitch_deploy@0, nova_initiated_odocks@0, nova_initiated_od1_nova_config@0, nova_initiated_od0_nova_pull@0, glance_initiated_odocks@0, glance_initiated_od0_glance_pull@0, keystone_initiated_odocks@0, keystone_initiated_od0_keystone_pull@0, mariadb_initiated_odocks@0, mariadb_initiated_od0_mariadb_pull@0, rabbitmq_initiated_odocks@0, rabbitmq_initiated_od0_rabbitmq_deploy@0, memcached_initiated_odocks@0, memcached_initiated_od0_memcached_deploy@0, haproxy_initiated_odocks@0, haproxy_initiated_od0_haproxy_deploy@0, common_initiated_odocks@0, common_initiated_od0_common_ktb_deploy@0, keystone_pulled_id0_keystone_pull@4, keystone_pulled_idocks@4, keystone_pulled_odocks@4, mariadb_pulled_id0_mariadb_pull@4, mariadb_pulled_idocks@4, mariadb_pulled_odocks@4, memcached_deployed_id0_memcached_deploy@4, memcached_deployed_idocks@4, glance_pulled_id0_glance_pull@5, common_ktb_deployed_id0_common_ktb_deploy@6, common_ktb_deployed_idocks@6, common_ktb_deployed_odocks@6, common_ktb_deployed_od0_common_deploy@6, neutron_pulled_id0_neutron_pull@7, nova_pulled_id0_nova_pull@8, rabbitmq_deployed_id0_rabbitmq_deploy@10, openvswitch_deployed_id0_openvswitch_deploy@10, openvswitch_deployed_idocks@10, rabbitmq_deployed_idocks@10, haproxy_deployed_id0_haproxy_deploy@19, haproxy_deployed_idocks@19, mariadb_pulled_od0_mariadb_deploy@19, nova_pulled_id1_nova_config@20, common_deployed_id0_common_deploy@31, common_deployed_idocks@31, mariadb_deployed_id0_mariadb_deploy@81, mariadb_deployed_idocks@81, neutron_initiated_od1_neutron_config@81, nova_initiated_od2_nova_create_db@81, glance_initiated_od1_glance_config@81, keystone_pulled_od0_keystone_deploy@81, glance_pulled_id1_glance_config@89, nova_pulled_id2_nova_create_db@94, nova_pulled_idocks@94, nova_pulled_odocks@94, nova_pulled_od1_nova_upgrade_api_db@94, nova_pulled_od0_nova_upgrade_db@94, neutron_pulled_id1_neutron_config@107, nova_ready_id1_nova_upgrade_api_db@138, keystone_deployed_id0_keystone_deploy@179, keystone_deployed_idocks@179, neutron_initiated_od2_neutron_register@179, nova_initiated_od3_nova_register@179, glance_initiated_od2_glance_register@179, nova_ready_id0_nova_upgrade_db@193, nova_ready_idocks@193, nova_ready_odocks@193, nova_ready_od0_nova_deploy@193, neutron_pulled_id2_neutron_register@200, neutron_pulled_idocks@200, neutron_pulled_odocks@200, neutron_pulled_od0_neutron_deploy@200, glance_pulled_id2_glance_register@203, glance_pulled_idocks@203, glance_pulled_odocks@203, glance_pulled_od0_glance_deploy@203, nova_deployed_id0_nova_register@230, glance_deployed_id0_glance_deploy@294, glance_deployed_idocks@294, nova_deployed_id1_nova_deploy@382, nova_deployed_idocks@382, neutron_deployed_id0_neutron_deploy@400, neutron_deployed_idocks@400
[info] Time: 307.8s (total) = 303.4s (user) + 4.4s (system)
[info] Max memory used: 4217.4Mo
* gantt boundaries max
[info] Checking mincost E (true U ((((( nova_deployed > 0 ) and ( keystone_deployed > 0 )) and ( neutron_deployed > 0 )) and ( glance_deployed > 0 )) and ( mariadb_deployed > 0 )))
[info] Computation mode is time
[info] Infimum cost computation uses simple cost state classes.
[info] Convergence method is merge on common markings.
=-423

Trace: neutron_initiated_odocks@0, neutron_initiated_od0_neutron_pull@0, openvswitch_initiated_odocks@0, openvswitch_initiated_od0_openvswitch_deploy@0, nova_initiated_odocks@0, nova_initiated_od1_nova_config@0, nova_initiated_od0_nova_pull@0, glance_initiated_odocks@0, glance_initiated_od0_glance_pull@0, keystone_initiated_odocks@0, keystone_initiated_od0_keystone_pull@0, mariadb_initiated_odocks@0, mariadb_initiated_od0_mariadb_pull@0, rabbitmq_initiated_odocks@0, rabbitmq_initiated_od0_rabbitmq_deploy@0, memcached_initiated_odocks@0, memcached_initiated_od0_memcached_deploy@0, haproxy_initiated_odocks@0, haproxy_initiated_od0_haproxy_deploy@0, common_initiated_odocks@0, common_initiated_od0_common_ktb_deploy@0, glance_pulled_id0_glance_pull@5, keystone_pulled_id0_keystone_pull@5, keystone_pulled_idocks@5, keystone_pulled_odocks@5, mariadb_pulled_id0_mariadb_pull@5, mariadb_pulled_idocks@5, mariadb_pulled_odocks@5, memcached_deployed_id0_memcached_deploy@5, memcached_deployed_idocks@5, neutron_pulled_id0_neutron_pull@7, common_ktb_deployed_id0_common_ktb_deploy@7, common_ktb_deployed_idocks@7, common_ktb_deployed_odocks@7, common_ktb_deployed_od0_common_deploy@7, nova_pulled_id0_nova_pull@8, rabbitmq_deployed_id0_rabbitmq_deploy@9, rabbitmq_deployed_idocks@9, openvswitch_deployed_id0_openvswitch_deploy@10, openvswitch_deployed_idocks@10, nova_pulled_id1_nova_config@20, haproxy_deployed_id0_haproxy_deploy@20, haproxy_deployed_idocks@20, mariadb_pulled_od0_mariadb_deploy@20, common_deployed_id0_common_deploy@32, common_deployed_idocks@32, mariadb_deployed_id0_mariadb_deploy@87, mariadb_deployed_idocks@87, neutron_initiated_od1_neutron_config@87, nova_initiated_od2_nova_create_db@87, glance_initiated_od1_glance_config@87, keystone_pulled_od0_keystone_deploy@87, glance_pulled_id1_glance_config@95, nova_pulled_id2_nova_create_db@100, nova_pulled_idocks@100, nova_pulled_odocks@100, nova_pulled_od1_nova_upgrade_api_db@100, nova_pulled_od0_nova_upgrade_db@100, neutron_pulled_id1_neutron_config@113, nova_ready_id1_nova_upgrade_api_db@144, keystone_deployed_id0_keystone_deploy@193, keystone_deployed_idocks@193, neutron_initiated_od2_neutron_register@193, nova_initiated_od3_nova_register@193, glance_initiated_od2_glance_register@193, nova_ready_id0_nova_upgrade_db@199, nova_ready_idocks@199, nova_ready_odocks@199, nova_ready_od0_nova_deploy@199, neutron_pulled_id2_neutron_register@220, neutron_pulled_idocks@220, neutron_pulled_odocks@220, neutron_pulled_od0_neutron_deploy@220, glance_pulled_id2_glance_register@220, glance_pulled_idocks@220, glance_pulled_odocks@220, glance_pulled_od0_glance_deploy@220, nova_deployed_id0_nova_register@244, glance_deployed_id0_glance_deploy@311, glance_deployed_idocks@311, nova_deployed_id1_nova_deploy@388, nova_deployed_idocks@388, neutron_deployed_id0_neutron_deploy@423, neutron_deployed_idocks@423
[info] Time: 297.5s (total) = 294.2s (user) + 3.3s (system)
[info] Max memory used: 3999.3Mo