import sys
sys.path.insert(0, '../../')
from gnuplot_gantt import *

tasks = [
    ("glance deploy",212,306),
    ("neutron deploy", 211,397),
    ("nova deploy",206,395),
    ("nova register", 187,237),
    ("glance register", 187,212),
    ("neutron register", 187,211),
    ("nova upgrade-db",106,206),
    ("nova upg-api-db",106,151),
    ("kst deploy", 92,187),
    ("nova create-db",92,106),
    ("neutron config",92,118),
    ("glance config", 92, 100),
    ("mariadb deploy",16,92),
    ("com deploy",7,33),
    ("nova config", 0,23),
    ("haproxy deploy",0,16),
    ("rabmq deploy",0,10),
    ("ovs deploy",0,11),
    ("nova pull",0,14),
    ("neutron pull",0,10),
    ("com ktb-deploy",0,7),
    ("glance pull", 0, 6),
    ("memc deploy", 0, 5),
    ("mariadb pull",0,4),
    ("kst pull",0,4)]

if __name__ == '__main__':
    gnuplot_file_from_list(tasks, "3_nova_gantt.gnu", title='')
